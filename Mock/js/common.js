//recipeAdd.html

//ファイルの選択
$('.custom-file-input').on('change',function(){
    $(this).next('.custom-file-label').html($(this)[0].files[0].name);
})

//ファイルの取消
$('.reset').click(function(){
    $(this).parent().prev().children('.custom-file-label').html('ファイル選択...');
    $('.custom-file-input').val('');
})


//recipeEdit.html
//選択した画像とファイル名を表示
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#dish').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

// カテゴリ名の追加
$(function() {
  // 追加ボタンをクリックしたら発動
  $('#categoryAdd').click(function() {
      
    $('#categorySelect').append( function(){
      var target = $('#newCategory').val();
      // 入力値が空でなく、未登録の値である場合にセレクトボックスに追加
      if(target[0] && $('#categorySelect option[value="'+target+'"]').length == 0) {
        return $('<option>').val(target).text(target);
      }
    });
  });
});

// チェックボックスの追加
$(function() {
  // 追加ボタンをクリックしたら発動
  $('#tagAdd').click(function() {
    
    // 入力したタグ名を変数に格納
    var addTag = $('#newTag').val();
    // appendで動的にチェックボックスを追加
    var html = '<div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" name="tag[]" id="'+addTag+'"><label class="custom-control-label" for="'+addTag+'">'+addTag+'</label></div>';
    $('.tag').append(html);
  });
});


//recipeDetail.html
// お気に入りボタン
$(function(){
    var toggleButton = $('.button-toggle');
    
    //aタグのclass内にキーを追加
    toggleButton.click(function() {
        if ($(this).hasClass('toggle-on')) {   
            toggleButton.removeClass('toggle-on');
            toggleButton.addClass('toggle-off');
        } else {
            toggleButton.removeClass('toggle-off');
            toggleButton.addClass('toggle-on');
        }
        return false;
    });
});


//recipeSort.html

var minCount = 1;
var maxCount = 6;

// 新規カテゴリの動的追加
$(function(){
    // 追加
    $('#ct-plus').on('click', function(){
      var inputCount = $('#ct-new-area .unit').length;
      if (inputCount < maxCount){
        // 末尾をイベントごと複製
        var element = $('#ct-new-area .unit:last-child').clone(true);
        // 複製したinputのクリア
        var inputList = element[0].querySelectorAll('input[type="text"]');
        for (var i = 0; i < inputList.length; i++) {
          inputList[i].value = "";
        }
        // 末尾追加
        $('#ct-new-area .unit').parent().append(element);

        $('#ct-new-area input[type="text"]').each(function(i){
            $(this).attr('name','newCt' + (i+1));
        });
      }
    });

    // 削除
    $('.ct-minus').on('click', function(){
      var inputCount = $('#ct-new-area .unit').length;
      if (inputCount > minCount){
        $(this).parents('.unit').remove();
      }
    });
});

// 新規タグの動的追加
$(function(){
    // 追加
    $('#tag-plus').on('click', function(){
      var inputCount = $('#tag-new-area .unit').length;
      if (inputCount < maxCount){
        // 末尾をイベントごと複製
        var element = $('#tag-new-area .unit:last-child').clone(true);
        // 複製したinputのクリア
        var inputList = element[0].querySelectorAll('input[type="text"]');
        for (var i = 0; i < inputList.length; i++) {
          inputList[i].value = "";
        }
        // 末尾追加
        $('#tag-new-area .unit').parent().append(element);

        $('#tag-new-area input[type="text"]').each(function(i){
            $(this).attr('name','newTag' + (i+1));
        });
      }
    });

    // 削除
    $('.tag-minus').on('click', function(){
      var inputCount = $('#tag-new-area .unit').length;
      if (inputCount > minCount){
        $(this).parents('.unit').remove();
      }
    });
});