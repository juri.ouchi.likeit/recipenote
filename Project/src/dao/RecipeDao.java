package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.RecipeBeans;

/**
 * t_recipeテーブル用のDao
 */

public class RecipeDao {

	/**
     * ユーザーIDから登録済の全レシピ情報を表示数ぶん取得
     * @param userId
	 * @param pageNum
	 * @param pageMaxRecipeCount
     * @return List<RecipeBeans> recipeList
     */
    public List<RecipeBeans> findAll(int userId, int pageNum, int pageMaxRecipeCount) {
        Connection conn = null;
        List<RecipeBeans> recipeList = new ArrayList<RecipeBeans>();

        try {
        	// ID順に何番目のレシピから表示するか
        	int startItemNum = (pageNum - 1) * pageMaxRecipeCount;

            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT recipe_id, recipe_name, image, description, favorite_id"
            		+ " FROM t_recipe LEFT OUTER JOIN t_favorite USING (recipe_id)"
            		+ " WHERE t_recipe.user_id = ? ORDER BY recipe_id DESC LIMIT ?, ?";

            // SELECTを実行し、表示数ぶんのレシピ情報を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            pStmt.setInt(2, startItemNum);
            pStmt.setInt(3, pageMaxRecipeCount);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // RecipeBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int recipeId = rs.getInt("recipe_id");
                String recipeName = rs.getString("recipe_name");
                String image = rs.getString("image");
                String description = rs.getString("description");
                int favoriteId = rs.getInt("favorite_id");

                RecipeBeans recipe = new RecipeBeans(recipeId, recipeName, image, description, favoriteId);
                recipeList.add(recipe);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        System.out.println("Getting Recipes by userId has been completed.");
        return recipeList;
    }


	/**
	 * 登録レシピ総数を取得
	 *
	 * @param userId
	 * @return allRecipeCnt
	 */
	public double getRecipeCount(int userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT count(recipe_id) AS cnt FROM t_recipe WHERE user_id = ?";

			// SELECTを実行し、登録レシピ総数を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
                return 0;
            }

			double allRecipeCnt = rs.getInt("cnt");
			System.out.println("allRecipeNum: " + allRecipeCnt);
			return allRecipeCnt;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * レシピの詳細とカテゴリ名を取得
	 *
	 * @param recipeId
	 * @return RecipeBeans(recipeName, image, description, servings, ingredients,
	 * 			 preparation, tips, source, categoryId, categoryName)
	 */
	public RecipeBeans getDetail(int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_recipe"
					+ " LEFT OUTER JOIN t_category USING (category_id)"
					+ " LEFT OUTER JOIN t_favorite USING (recipe_id)"
					+ " WHERE t_recipe.recipe_id = ?";

			// SELECTを実行し、レシピの詳細を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, recipeId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
                return null;
            }

            // インスタンスのフィールドに追加
			String recipeName = rs.getString("recipe_name");
			String image = rs.getString("image");
			String description = rs.getString("description");
			String servings = rs.getString("servings");
            String ingredients = rs.getString("ingredients");
            String preparation = rs.getString("preparation");
            String tips = rs.getString("tips");
            String source = rs.getString("source");
            int categoryId = rs.getInt("category_id");
            String categoryName = rs.getString("category_name");
            int favoriteId = rs.getInt("favorite_id");

            System.out.println("Getting detail of the recipe by recipeId has been completed.");
			return new RecipeBeans(recipeId, recipeName, image, description, servings,
					 ingredients, preparation, tips, source, categoryId, categoryName, favoriteId);

		} catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
     * カテゴリIDから紐づけられた全レシピ情報を表示数ぶん取得
     * @param categoryId
	 * @param pageNum
	 * @param pageMaxRecipeCount
     * @return List<RecipeBeans> recipeList
     */
    public List<RecipeBeans> findAllByCategory(int categoryId, int pageNum, int pageMaxRecipeCount) {
        Connection conn = null;
        List<RecipeBeans> recipeList = new ArrayList<RecipeBeans>();

        try {
        	// ID順に何番目のレシピから表示するか
        	int startItemNum = (pageNum - 1) * pageMaxRecipeCount;

            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT recipe_id, recipe_name, image, description, favorite_id"
            		+ " FROM t_recipe"
            		+ " LEFT OUTER JOIN t_favorite"
            		+ " USING (recipe_id)"
            		+ " WHERE category_id = ?"
            		+ " ORDER BY recipe_id DESC LIMIT ?, ?";

            // SELECTを実行し、表示数ぶんのレシピ情報を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, categoryId);
            pStmt.setInt(2, startItemNum);
            pStmt.setInt(3, pageMaxRecipeCount);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // RecipeBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int recipeId = rs.getInt("recipe_id");
                String recipeName = rs.getString("recipe_name");
                String image = rs.getString("image");
                String description = rs.getString("description");
                int favoriteId = rs.getInt("favorite_id");

                RecipeBeans recipe = new RecipeBeans(recipeId, recipeName, image, description, favoriteId);
                recipeList.add(recipe);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Getting Recipes by categoryId has been completed.");
        return recipeList;
    }

	/**
	 * カテゴリIDに紐づいたレシピ総数を取得
	 *
	 * @param categoryId
	 * @return allRecipeCnt
	 */
	public double getRecipeCountByCategory(int categoryId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT count(recipe_id) AS cnt FROM t_recipe WHERE category_id = ?";

			// SELECTを実行し、登録レシピ総数を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, categoryId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
                return 0;
            }

			double allRecipeCnt = rs.getInt("cnt");
			System.out.println("allRecipeNumByCategory: " + allRecipeCnt);
			return allRecipeCnt;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	/**
	 * 新しいレシピを登録（カテゴリ、タグ情報以外）
	 *
	 * @param
	 * @return autoIncKey（登録したばかりのrecipeId）
	 */
	public int saveRecipe(String recipeName, String image, String description, String servings,
			String ingredients, String preparation, String tips, String source, int userId) {
		Connection conn = null;
		int autoIncKey = -1;

		try {
			// 料理写真がない場合、NO IMAGE画像を登録
			if(image.equals("")) {
				image = "no-image2.png";
            }

			// データベースへ接続
            conn = DBManager.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO t_recipe ( recipe_name, image, description," +
            		" servings, ingredients, preparation, tips, source," +
            		" user_id, category_id )" +
            		" SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, category_id" +
            		" FROM t_category" +
            		" WHERE user_id = ? AND category_name = '未分類'";

            // INSERTを実行
            PreparedStatement pStmt = conn.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
            pStmt.setString(1, recipeName);
            pStmt.setString(2, image);
            pStmt.setString(3, description);
            pStmt.setString(4, servings);
            pStmt.setString(5, ingredients);
            pStmt.setString(6, preparation);
            pStmt.setString(7, tips);
            pStmt.setString(8, source);
            pStmt.setInt(9, userId);
            pStmt.setInt(10, userId);

            pStmt.executeUpdate();

            ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * 新しいレシピを登録（タグ情報以外）
	 *
	 * @param
	 * @return autoIncKey（登録したばかりのrecipeId）
	 */
	public int saveRecipe(String recipeName, String image, String description, String servings,
			String ingredients, String preparation, String tips, String source, int categoryId, int userId) {
		Connection conn = null;
		int autoIncKey = -1;

		try {
			// 料理写真がない場合、NO IMAGE画像を登録
			if(image.equals("")) {
				image = "no-image2.png";
            }

			// ポイントがない場合、「なし」と登録
			if(tips.equals("")) {
				tips = "なし";
			}

			// データベースへ接続
            conn = DBManager.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO t_recipe ( recipe_name, image, description," +
            		" servings, ingredients, preparation, tips, source," +
            		" user_id, category_id )" +
            		" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

            // INSERTを実行
            PreparedStatement pStmt = conn.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
            pStmt.setString(1, recipeName);
            pStmt.setString(2, image);
            pStmt.setString(3, description);
            pStmt.setString(4, servings);
            pStmt.setString(5, ingredients);
            pStmt.setString(6, preparation);
            pStmt.setString(7, tips);
            pStmt.setString(8, source);
            pStmt.setInt(9, userId);
            pStmt.setInt(10, categoryId);

            pStmt.executeUpdate();

            ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * 対象カテゴリに紐づいたレシピを未分類カテゴリに紐づけ直す
	 *
	 * @param
	 */
	public void relinkToRoot(int userId, String[] categoryIds) {
		Connection conn = null;

		try {
			// データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE t_recipe"
            		+ " SET category_id = (SELECT category_id FROM t_category WHERE user_id = ? AND category_name = '未分類')"
            		+ " WHERE category_id = ?";

			if (categoryIds.length > 1) {
				for (int i = 1; i < categoryIds.length; i++) {
					sql += " OR category_id = ?";
				}
			}

			// UPDATEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			for (int i = 0; i < categoryIds.length; i++) {
				pStmt.setInt(i+2, Integer.parseInt(categoryIds[i]));
			}

			pStmt.executeUpdate();
			System.out.println("Relinking recipes to root category has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * レシピを削除（レシピテーブルとレシピタグテーブルとお気に入りテーブルから一括）
	 *
	 * @param
	 */
	public void deleteRecipe(int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
            conn = DBManager.getConnection();

            //TODO DELETE文を準備
            String sql = "DELETE t_recipe, t_recipe_tag, t_favorite"
            		+ " FROM t_recipe"
            		+ " LEFT OUTER JOIN t_recipe_tag"
            		+ " USING (recipe_id)"
            		+ " LEFT OUTER JOIN t_favorite"
            		+ " USING (recipe_id)"
            		+ " WHERE recipe_id = ?";

			// DELETEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, recipeId);
			pStmt.executeUpdate();
			System.out.println("Deleting recipe and recipe-tags and favorite has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
     * レシピを検索
     *
     * @param
     * @return List<RecipeBeans> recipeList
     */
    public List<RecipeBeans> searchRecipe(int userId, String recipeWord, String categoryId,
    		String[] tagIds, String[] favs, int pageNum, int pageMaxRecipeCount) {
        Connection conn = null;
        List<RecipeBeans> recipeList = new ArrayList<RecipeBeans>();

        try {
        	String[] recipeWords = null;

        	// ID順に何番目のレシピから表示するか
        	int startItemNum = (pageNum - 1) * pageMaxRecipeCount;

            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
//            String sqlOld = "SELECT DISTINCT favorite_id, recipe_id, recipe_name, image, description"
//            		+ " FROM t_recipe r"
//            		+ " LEFT OUTER JOIN t_recipe_tag rt"
//            		+ " USING (recipe_id)"
//            		+ " LEFT OUTER JOIN t_favorite"
//            		+ " USING (recipe_id)"
//            		+ " WHERE r.user_id = " + userId;

            String sql = "SELECT DISTINCT favorite_id, r.recipe_id, recipe_name, image, description"
            		+ " FROM t_recipe r"
            		+ " LEFT OUTER JOIN t_favorite f"
            		+ " ON r.recipe_id = f.recipe_id";

            String sqlWhere = " WHERE r.user_id = " + userId;

            // 引数の入力値が空欄でなければSQL文を足していく
            if(tagIds != null && tagIds.length > 0) {
            	sql += " INNER JOIN t_recipe_tag rt0 ON r.recipe_id = rt0.recipe_id AND rt0.tag_id = " + Integer.parseInt(tagIds[0]);

            	if (tagIds.length > 1) {
    				for (int i = 1; i < tagIds.length; i++) {
    					sql += " INNER JOIN t_recipe_tag rt"+i+" ON r.recipe_id = rt"+i+".recipe_id AND rt"+i+".tag_id = " + Integer.parseInt(tagIds[i]);
    			    }
    			}
            }

            if(recipeWord != null && !recipeWord.equals("")) {
            	// 複数語句検索用の処理
            	// 検索ワードの全角空白を半角空白に揃える
            	String recipeWordRep = recipeWord.replace("　", " ");
            	// 半角空白で分割して配列にする
            	recipeWords = recipeWordRep.split(" ", 0);

            	for (int i = 0; i < recipeWords.length; i++){
            		sqlWhere += " AND recipe_name LIKE ?";
            	}
            }

            if(categoryId != null && !categoryId.equals("")) {
            	sqlWhere += " AND category_id = " + Integer.parseInt(categoryId);
            }

            if(favs != null && favs.length > 0) {
            	sqlWhere += " AND favorite_id <> 0";
            }

            sqlWhere +=  " ORDER BY r.recipe_id DESC LIMIT " + startItemNum + ", " + pageMaxRecipeCount;
            sql += sqlWhere;

            // SELECTを実行し、表示数ぶんのレシピ情報を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);

            if(recipeWords != null && recipeWords.length > 0){
            	for (int i = 0; i < recipeWords.length; i++){
            		pStmt.setString(i+1, "%" + recipeWords[i] +"%");
            	}
            }

            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // RecipeBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int recipeId = rs.getInt("recipe_id");
                String recipeName = rs.getString("recipe_name");
                String image = rs.getString("image");
                String description = rs.getString("description");
                int favoriteId = rs.getInt("favorite_id");

                RecipeBeans recipe = new RecipeBeans(recipeId, recipeName, image, description, favoriteId);
                recipeList.add(recipe);
            }

            System.out.println("Searching Recipes has been completed.");
            return recipeList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	/**
	 * フォーム検索対象のレシピ総数を取得
	 *
	 * @param
	 * @return allRecipeCnt
	 */
	public double getRecipeCountByForm(int userId, String recipeWord, String categoryId,
    		String[] tagIds, String[] favs) {
		Connection conn = null;

		try {
			String[] recipeWords = null;

			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
//			String sqlOld = "SELECT count(DISTINCT recipe_id) AS cnt"
//					+ " FROM t_recipe r"
//					+ " LEFT OUTER JOIN t_recipe_tag rt"
//					+ " USING (recipe_id)"
//					+ " LEFT OUTER JOIN t_favorite"
//					+ " USING (recipe_id)"
//					+ " WHERE r.user_id = " + userId;

			String sql = "SELECT count(DISTINCT r.recipe_id) AS cnt"
            		+ " FROM t_recipe r"
            		+ " LEFT OUTER JOIN t_favorite f"
            		+ " ON r.recipe_id = f.recipe_id";

            String sqlWhere = " WHERE r.user_id = " + userId;

            // 引数の入力値が空欄でなければSQL文を足していく
            if(tagIds != null && tagIds.length > 0) {
            	sql += " INNER JOIN t_recipe_tag rt0 ON r.recipe_id = rt0.recipe_id AND rt0.tag_id = " + Integer.parseInt(tagIds[0]);

            	if (tagIds.length > 1) {
    				for (int i = 1; i < tagIds.length; i++) {
    					sql += " INNER JOIN t_recipe_tag rt"+i+" ON r.recipe_id = rt"+i+".recipe_id AND rt"+i+".tag_id = " + Integer.parseInt(tagIds[i]);
    			    }
    			}
            }

            if(recipeWord != null && !recipeWord.equals("")) {
            	// 複数語句検索用の処理
            	// 検索ワードの全角空白を半角空白に揃える
            	String recipeWordRep = recipeWord.replace("　", " ");
            	// 半角空白で分割して配列にする
            	recipeWords = recipeWordRep.split(" ", 0);

            	for (int i = 0; i < recipeWords.length; i++){
            		sqlWhere += " AND recipe_name LIKE ?";
            	}
            }

            if(categoryId != null && !categoryId.equals("")) {
            	sqlWhere += " AND category_id = " + Integer.parseInt(categoryId);
            }

            if(favs != null && favs.length > 0) {
            	sqlWhere += " AND favorite_id <> 0";
            }

            sql += sqlWhere;

            // SELECTを実行し、対象レシピ総数を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);

            if(recipeWords != null && recipeWords.length > 0){
            	for (int i = 0; i < recipeWords.length; i++){
            		pStmt.setString(i+1, "%" + recipeWords[i] +"%");
            	}
            }

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
                return 0;
            }

			double allRecipeCnt = rs.getInt("cnt");
			System.out.println("allRecipeNumByForm: " + allRecipeCnt);
			return allRecipeCnt;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	/**
	 * レシピ情報を更新（タグ情報以外）
	 *
	 * @param
	 */
	public void updateRecipe(int recipeId, String recipeName, String image, String description, String servings,
			String ingredients, String preparation, String tips, String source, int userId, int categoryId) {
		Connection conn = null;

		try {
			// 料理写真がない場合、NO IMAGE画像を登録
			//if(image.equals("")) {
			//	image = "no-image2.png";
            //}

			// ポイントがない場合、「なし」と登録
			if(tips.equals("")) {
				tips = "なし";
			}

			// データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE t_recipe"
            		+ " SET recipe_name = ?,"
            		+ " image = ?,"
            		+ " description = ?,"
            		+ " servings = ?,"
            		+ " ingredients = ?,"
            		+ " preparation = ?,"
            		+ " tips = ?,"
            		+ " source = ?,"
            		+ " user_id = "+ userId +","
            		+ " category_id = " + categoryId
            		+ " WHERE recipe_id = " + recipeId;

            // UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, recipeName);
            pStmt.setString(2, image);
            pStmt.setString(3, description);
            pStmt.setString(4, servings);
            pStmt.setString(5, ingredients);
            pStmt.setString(6, preparation);
            pStmt.setString(7, tips);
            pStmt.setString(8, source);

            pStmt.executeUpdate();

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * レシピ情報を更新（画像、タグ情報以外）
	 *
	 * @param
	 */
	public void updateRecipe(int recipeId, String recipeName, String description, String servings,
			String ingredients, String preparation, String tips, String source, int userId, int categoryId) {
		Connection conn = null;

		try {
			// ポイントがない場合、「なし」と登録
			if(tips.equals("")) {
				tips = "なし";
			}

			// データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE t_recipe"
            		+ " SET recipe_name = ?,"
            		+ " description = ?,"
            		+ " servings = ?,"
            		+ " ingredients = ?,"
            		+ " preparation = ?,"
            		+ " tips = ?,"
            		+ " source = ?,"
            		+ " user_id = "+ userId +","
            		+ " category_id = " + categoryId
            		+ " WHERE recipe_id = " + recipeId;

            // UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, recipeName);
            pStmt.setString(2, description);
            pStmt.setString(3, servings);
            pStmt.setString(4, ingredients);
            pStmt.setString(5, preparation);
            pStmt.setString(6, tips);
            pStmt.setString(7, source);

            pStmt.executeUpdate();

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
