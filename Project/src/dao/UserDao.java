package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import controller.RecipenoteHelper;
import model.UserBeans;

/**
 * t_userテーブル用のDao
 */

public class UserDao {

	/**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     *
     * @param
     * @return UserBeans(loginIdData, nameData)
     */
    public UserBeans findByLoginInfo(String loginId, String password) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM t_user WHERE login_id = ? and password = ?";

            // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, RecipenoteHelper.getMd5(password));
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int userIdData = rs.getInt("user_id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("user_name");

            System.out.println("Searching userInfo by loginId has been completed.");
            return new UserBeans(userIdData, loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    /**
     * ユーザーに紐づく情報を全削除
     *
     * @param
     */
    public void deleteUser(int userId) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // DELETE文を準備
            String sql = "DELETE t_user, t_recipe, t_category, t_recipe_tag, t_tag, t_favorite"
            		+ " FROM t_user"
            		+ " LEFT OUTER JOIN t_recipe"
            		+ " USING (user_id)"
            		+ " LEFT OUTER JOIN t_category"
            		+ " USING (user_id)"
            		+ " LEFT OUTER JOIN t_recipe_tag"
            		+ " USING (recipe_id)"
            		+ " LEFT OUTER JOIN t_tag"
            		+ " USING (user_id)"
            		+ " LEFT OUTER JOIN t_favorite"
            		+ " USING (user_id)"
            		+ " WHERE user_id = ?";

            // DELETEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            pStmt.executeUpdate();

            System.out.println("Deleting userInfo has been completed.");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ユーザー情報を更新（パスワード含まない）
     *
     * @param
     */
    public void updateUser(int userId, String userName) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE t_user SET user_name = ?, update_date = NOW() WHERE user_id = ?";

            // UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userName);
            pStmt.setInt(2, userId);
            pStmt.executeUpdate();

            System.out.println("Updating userName has been completed.");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ユーザー情報を更新（パスワード含む）
     *
     * @param
     */
    public void updateUser(int userId, String userName, String password) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE t_user SET user_name = ?, password = MD5(?), update_date = NOW() WHERE user_id = ?";

            // UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userName);
            pStmt.setString(2, password);
            pStmt.setInt(3, userId);
            pStmt.executeUpdate();

            System.out.println("Updating userInfo has been completed.");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	/**
     * ログインID重複チェック（ログインID(仮)からユーザーIDを検索）
     * @param
     * @return int userId
     */
    public int checkLoginId(String loginId) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT user_id FROM t_user WHERE login_id = ?";

            // SELECTを実行し、カテゴリIDを取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return 0;
            }

            int userId = rs.getInt("user_id");

            System.out.println("Searching userId by loginId has been completed.");
            return userId;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 新しいユーザー情報を登録
     *
     * @param
     * @return autoIncKey（登録したばかりのuserId）
     */
    public int createUser(String loginId, String userName, String password) {
        Connection conn = null;
        int autoIncKey = -1;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO t_user (login_id, user_name, password, create_date, update_date) VALUES (?, ?, MD5(?), NOW(), NOW())";

            // INSERTを実行
            PreparedStatement pStmt = conn.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
            pStmt.setString(1, loginId);
            pStmt.setString(2, userName);
            pStmt.setString(3, password);
            pStmt.executeUpdate();

            ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			System.out.println("Registing new user has been completed.");
			return autoIncKey;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
