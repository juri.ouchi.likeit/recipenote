package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.TagBeans;

/**
 * t_tagテーブル用のDao
 */

public class TagDao {

	/**
     * ユーザーIDから登録タグ名を取得
     * @param userId
     * @return List<TagBeans> tagList
     */
    public List<TagBeans> getTagName(int userId) {
        Connection conn = null;
        List<TagBeans> tagList = new ArrayList<TagBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM t_tag WHERE user_id = ?";

            // SELECTを実行し、カテゴリ情報を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // TagBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int tagId = rs.getInt("tag_id");
                String tagName = rs.getString("tag_name");

                TagBeans tag = new TagBeans(tagId, tagName);
                tagList.add(tag);
            }

            System.out.println("Getting tags by userId has been completed.");
            return tagList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	/**
     * タグ名重複チェック（ユーザーIDとタグ名(仮)からタグIDを検索）
     * @param
     * @return int tagId
     */
    public int checkTagName(String tagName, int userId) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT tag_id FROM t_tag WHERE user_id = ? AND tag_name = ?";

            // SELECTを実行し、カテゴリIDを取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            pStmt.setString(2, tagName);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return 0;
            }

            int tagId = rs.getInt("tag_id");

            System.out.println("Searching tagId by tagName and userId has been completed.");
            return tagId;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

	/**
	 * 新しいタグを登録
	 *
	 * @param
	 */
	public void addNewTag(String tagName, int userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_tag (tag_name, user_id) VALUES (?, ?)";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, tagName);
			pStmt.setInt(2, userId);
			pStmt.executeUpdate();

			System.out.println("Add new tag has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * 新しいタグを一括登録
	 *
	 * @param
	 */
	public void addNewTag(List<String> tagList, int userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_tag (tag_name, user_id) VALUES (?, " + userId + ")";

			if (tagList.size() > 1) {
				for (int i = 1; i < tagList.size(); i++) {
					sql += ", (?, " + userId + ")";
				}
			}

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			for (int i = 0; i < tagList.size(); i++) {
				pStmt.setString(i+1, tagList.get(i));
			}

			pStmt.executeUpdate();
			System.out.println("Add new tags has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * タグを削除（タグテーブルとレシピタグテーブルから一括）
	 *
	 * @param
	 */
	public void deleteTag(String[] tagIds) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// DELETE文を準備
			String sql = "DELETE t_tag, t_recipe_tag FROM t_tag LEFT OUTER JOIN t_recipe_tag USING (tag_id) WHERE tag_id = " + Integer.parseInt(tagIds[0]);

			if (tagIds.length > 1) {
				for (int i = 1; i < tagIds.length; i++) {
					sql += " OR tag_id = " + Integer.parseInt(tagIds[i]);
			    }
			}

			// DELETEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.executeUpdate();
			System.out.println("Deleting tags and recipe-tags has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * タグ名を更新
	 *
	 * @param
	 */
	public void updateTag(List<TagBeans> tagList) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE t_tag"
					+ " SET tag_name ="
					+ " CASE tag_id";

			sql += " WHEN " + tagList.get(0).getTagId() + " THEN ?";
			String sqlEnd = " END WHERE tag_id IN (" + tagList.get(0).getTagId();

			for (int i = 1; i < tagList.size(); i++) {
				sql += " WHEN " + tagList.get(i).getTagId() + " THEN ?";
				sqlEnd += ", " + tagList.get(i).getTagId();
			}

			sqlEnd += ")";
			sql += sqlEnd;

			// UPDATEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			for (int i = 0; i < tagList.size(); i++) {
				pStmt.setString(i+1, tagList.get(i).getTagName());
			}

			pStmt.executeUpdate();
			System.out.println("Updating tags has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
