package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.RecipeTagBeans;

/**
 * t_recipe_tagテーブル用のDao
 */

public class RecipeTagDao {

	/**
     * レシピIDから紐づけられたタグ情報を取得
     * @param recipeId
     * @return List<RecipeTagBeans> tagList
     */
    public List<RecipeTagBeans> getLinkedTags(int recipeId) {
        Connection conn = null;
        List<RecipeTagBeans> tagList = new ArrayList<RecipeTagBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM t_recipe_tag LEFT OUTER JOIN t_tag"
            		+ " USING (tag_id) WHERE t_recipe_tag.recipe_id = ?";

            // SELECTを実行し、タグ情報を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, recipeId);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // RecipeTagBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int tagId = rs.getInt("tag_id");
                String tagName = rs.getString("tag_name");

                RecipeTagBeans tag = new RecipeTagBeans(tagId, tagName);
                tagList.add(tag);
            }

            System.out.println("Getting linked tags by recipeId has been completed.");
            return tagList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	/**
     * タグIDから紐づけられた全レシピ情報を表示数ぶん取得
     * @param tagId
	 * @param pageNum
	 * @param pageMaxRecipeCount
     * @return List<RecipeTagBeans> recipeList
     */
    public List<RecipeTagBeans> findAllByTag(int tagId, int pageNum, int pageMaxRecipeCount) {
        Connection conn = null;
        List<RecipeTagBeans> recipeList = new ArrayList<RecipeTagBeans>();

        try {
        	// ID順に何番目のレシピから表示するか
        	int startItemNum = (pageNum - 1) * pageMaxRecipeCount;

            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT recipe_id, recipe_name, image, description, favorite_id"
            		+ " FROM t_recipe_tag"
            		+ " LEFT OUTER JOIN t_recipe"
            		+ " USING (recipe_id)"
            		+ " LEFT OUTER JOIN t_favorite"
            		+ " USING (recipe_id)"
            		+ " WHERE t_recipe_tag.tag_id = ?"
            		+ " ORDER BY recipe_id DESC LIMIT ?, ?";

            // SELECTを実行し、表示数ぶんのレシピ情報を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, tagId);
            pStmt.setInt(2, startItemNum);
            pStmt.setInt(3, pageMaxRecipeCount);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // RecipeTagBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int recipeId = rs.getInt("recipe_id");
                String recipeName = rs.getString("recipe_name");
                String image = rs.getString("image");
                String description = rs.getString("description");
                int favoriteId = rs.getInt("favorite_id");

                RecipeTagBeans recipe = new RecipeTagBeans(recipeId, recipeName, image, description, tagId, favoriteId);
                recipeList.add(recipe);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        System.out.println("Getting Recipes by tagId has been completed.");
        return recipeList;
    }

	/**
	 * タグIDに紐づいたレシピ総数を取得
	 *
	 * @param tagId
	 * @return allRecipeCnt
	 */
	public double getRecipeCountByTag(int tagId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT count(recipe_id) AS cnt FROM t_recipe_tag WHERE tag_id = ?";

			// SELECTを実行し、登録レシピ総数を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, tagId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
                return 0;
            }

			double allRecipeCnt = rs.getInt("cnt");
			System.out.println("allRecipeNumByTag: " + allRecipeCnt);
			return allRecipeCnt;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	/**
	 * レシピにタグを紐づけて登録
	 *
	 * @param
	 */
	public void addLinkingTag(int recipeId, String[] tagIds) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_recipe_tag (recipe_id, tag_id) VALUES (" + recipeId + ", "+ Integer.parseInt(tagIds[0]) +")";

			if (tagIds.length > 1) {
				for (int i = 1; i < tagIds.length; i++) {
					sql += " ,(" + recipeId + ", " + Integer.parseInt(tagIds[i]) + ")";
			    }
			}

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.executeUpdate();
			/*
			for (int i = 0; i < tags.length; i++) {
				pStmt.setInt(i*2+1, recipeId);
				pStmt.setInt(i*2+2, Integer.parseInt(tags[i]));
			}
			*/
			System.out.println("Add linking tags has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * 対象レシピに紐づくレコードをレシピタグテーブルから削除
	 *
	 * @param
	 */
	public void deleteRecipeTag(int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// DELETE文を準備
			String sql = "DELETE FROM t_recipe_tag WHERE recipe_id = ?";

			// DELETEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, recipeId);
			pStmt.executeUpdate();
			System.out.println("Deleting recipe-tags has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
