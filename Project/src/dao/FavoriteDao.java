package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * t_favoriteテーブル用のDao
 */

public class FavoriteDao {

	/**
	 * お気に入り登録有無を検索
	 *
	 * @param
	 * @return favoriteId
	 */
	public int searchFav(int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_favorite WHERE recipe_id = ?";

			// SELECTを実行してお気に入り登録の有無を検索
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, recipeId);

			// SELECTを実行し、カテゴリIDを取得
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return 0;
            }

            int favoriteId = rs.getInt("favorite_id");

			System.out.println("Searching favoriteId has been completed.");
			return favoriteId;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * お気に入り登録
	 *
	 * @param
	 */
	public void addFav(int userId, int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_favorite (user_id, recipe_id) VALUES (?, ?)";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setInt(2, recipeId);
			pStmt.executeUpdate();

			System.out.println("Adding favorite has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * お気に入り削除
	 *
	 * @param
	 */
	public void deleteFav(int favoriteId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// DELETE文を準備
			String sql = "DELETE FROM t_favorite WHERE favorite_id = ?";

			// DELETEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, favoriteId);
			pStmt.executeUpdate();

			System.out.println("Deleting favorite has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
