package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.CategoryBeans;

/**
 * t_categoryテーブル用のDao
 */

public class CategoryDao {

	/**
     * ユーザーIDから登録カテゴリ名を取得
     * @param userId
     * @return List<CategoryBeans> categoryList
     */
    public List<CategoryBeans> getCategoryName(int userId) {
        Connection conn = null;
        List<CategoryBeans> categoryList = new ArrayList<CategoryBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM t_category WHERE user_id = ?";

            // SELECTを実行し、カテゴリ名を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // CategoryBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int categoryId = rs.getInt("category_id");
                String categoryName = rs.getString("category_name");

                CategoryBeans category = new CategoryBeans(categoryId, categoryName);
                categoryList.add(category);
            }

            System.out.println("Getting categories by userId has been completed.");
            return categoryList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

	/**
     * ユーザーIDから"未分類以外の"登録カテゴリ名を取得
     * @param userId
     * @return List<CategoryBeans> categoryList
     */
    public List<CategoryBeans> getCategoryNameExceptRoot(int userId) {
        Connection conn = null;
        List<CategoryBeans> categoryList = new ArrayList<CategoryBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM t_category WHERE user_id = ? AND category_name <> '未分類'";

            // SELECTを実行し、カテゴリ名を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // CategoryBeansインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int categoryId = rs.getInt("category_id");
                String categoryName = rs.getString("category_name");

                CategoryBeans category = new CategoryBeans(categoryId, categoryName);
                categoryList.add(category);
            }

            System.out.println("Getting categories except root by userId has been completed.");
            return categoryList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

	/**
     * カテゴリ名重複チェック（ユーザーIDとカテゴリ名(仮)からカテゴリIDを検索）
     * @param
     * @return int categoryId
     */
    public int checkCategoryName(String categoryName, int userId) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT category_id FROM t_category WHERE user_id = ? AND category_name = ?";

            // SELECTを実行し、カテゴリIDを取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, userId);
            pStmt.setString(2, categoryName);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return 0;
            }

            int categoryId = rs.getInt("category_id");

            System.out.println("Searching categoryId by categoryName and userId has been completed.");
            return categoryId;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	/**
	 * 新しいカテゴリを登録
	 *
	 * @param
	 */
	public void addNewCategory(String categoryName, int userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_category (category_name, user_id) VALUES (?, ?)";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, categoryName);
			pStmt.setInt(2, userId);
			pStmt.executeUpdate();

			System.out.println("Adding new category has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * 新しいカテゴリを一括登録
	 *
	 * @param
	 */
	public void addNewCategory(List<String> categoryList, int userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_category (category_name, user_id) VALUES (?, " + userId + ")";

			if (categoryList.size() > 1) {
				for (int i = 1; i < categoryList.size(); i++) {
					sql += ", (?, " + userId + ")";
				}
			}

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			for (int i = 0; i < categoryList.size(); i++) {
				pStmt.setString(i+1, categoryList.get(i));
			}

			pStmt.executeUpdate();
			System.out.println("Adding new categories has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * カテゴリを削除
	 *
	 * @param
	 */
	public void deleteCategory(String[] categoryIds) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// DELETE文を準備
			String sql = "DELETE FROM t_category WHERE category_id = ?";

			if (categoryIds.length > 1) {
				for (int i = 1; i < categoryIds.length; i++) {
					sql += " OR category_id = ?";
				}
			}

			// DELETEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			for (int i = 0; i < categoryIds.length; i++) {
				pStmt.setInt(i+1, Integer.parseInt(categoryIds[i]));
			}

			pStmt.executeUpdate();
			System.out.println("Deleting categories has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * カテゴリ名を更新
	 *
	 * @param
	 */
	public void updateCategory(List<CategoryBeans> categoryList) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE t_category"
					+ " SET category_name ="
					+ " CASE category_id";

			sql += " WHEN " + categoryList.get(0).getCategoryId() + " THEN ?";
			String sqlEnd = " END WHERE category_id IN (" + categoryList.get(0).getCategoryId();

			for (int i = 1; i < categoryList.size(); i++) {
				sql += " WHEN " + categoryList.get(i).getCategoryId() + " THEN ?";
				sqlEnd += ", " + categoryList.get(i).getCategoryId();
			}

			sqlEnd += ")";
			sql += sqlEnd;

			// UPDATEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			for (int i = 0; i < categoryList.size(); i++) {
				pStmt.setString(i+1, categoryList.get(i).getCategoryName());
			}

			pStmt.executeUpdate();
			System.out.println("Updating categories has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	/**
	 * 未分類カテゴリを登録＜ユーザー新規登録時のみ＞
	 *
	 * @param
	 */
	public void addRootCategory(int userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO t_category (category_name, user_id) VALUES ('未分類', ?)";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.executeUpdate();

			System.out.println("Adding root category has been completed.");

		} catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
