package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CategoryDao;
import dao.RecipeDao;
import dao.TagDao;
import model.CategoryBeans;
import model.RecipeBeans;
import model.TagBeans;
import model.UserBeans;

@WebServlet("/RecipeListServlet")
public class RecipeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストスコープに指定されたページ番号がなければ1ページ目を表示
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null || request.getParameter("page_num").equals("") ? "1" : request.getParameter("page_num"));

		// レシピ一覧情報を取得
		RecipeDao recipeDao = new RecipeDao();
		List<RecipeBeans> recipeList = recipeDao.findAll(userId, pageNum, RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);

		// セッションに検索用パーツが入っていたら破棄
		RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

		// ユーザーが登録しているレシピの総数を取得、表示数で割って総ページ数を出す
		double allRecipeCount = recipeDao.getRecipeCount(userId);
		int pageMax = (int) Math.ceil(allRecipeCount / RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);
		System.out.println("レシピ総数:" + allRecipeCount + "、総ページ数：" + pageMax);

		// ユーザーが登録しているカテゴリ名とタグ名を取得
		CategoryDao categoryDao = new CategoryDao();
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);

		TagDao tagDao = new TagDao();
		List<TagBeans> tagList = tagDao.getTagName(userId);

		// リクエストスコープに下記をセット
		// 総ページ数、表示ページ番号、レシピ一覧情報、カテゴリ名、タグ名
		request.setAttribute("pageMax", pageMax);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("recipeList", recipeList);
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("tagList", tagList);

		// TOP画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
