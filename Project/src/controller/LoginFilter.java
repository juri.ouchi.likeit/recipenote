package controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.UserBeans;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/*")
public class LoginFilter implements Filter {

    /**
     * Default constructor.
     */
    public LoginFilter() {
        // Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try{
			// 引数をキャスト
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;

			HttpSession session = req.getSession();
			UserBeans userSession = (UserBeans) session.getAttribute("userInfo");

			String contextPath = req.getContextPath(); /* /Recipenote */
			//System.out.println(contextPath);
			String uri = req.getRequestURI(); /* /Recipenote/LoginServlet */
			//System.out.println(uri);

			// ログイン状態およびログインページ、新規登録ページ、静的リソースへのアクセス時は何もしない
		    boolean loggedIn = (session != null) && (userSession != null);
		    boolean loginRequest = uri.equals(contextPath + "/LoginServlet");
		    boolean isRegistPage = uri.equals(contextPath + "/UserCreateServlet");
		    boolean isCss = uri.startsWith(contextPath + "/css");
		    boolean isImage = uri.startsWith(contextPath + "/image");
		    boolean isJs = uri.startsWith(contextPath + "/js");

		    if (loggedIn || loginRequest || isRegistPage || isCss || isImage || isJs) {
		    	// pass the request along the filter chain
		        chain.doFilter(request, response);

		    } else {
		    	// ログイン画面にリダイレクトさせる
		        res.sendRedirect("LoginServlet");
		    }

			//((HttpServletResponse)response).sendRedirect("/Login");

		} catch (ServletException se){
			se.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// Auto-generated method stub
	}

}
