package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RecipeDao;
import dao.RecipeTagDao;
import model.RecipeBeans;
import model.RecipeTagBeans;

@WebServlet("/RecipeDetailServlet")
public class RecipeDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeDetailServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてレシピIDを受け取って型変換
		int recipeId = Integer.parseInt(request.getParameter("recipe_id"));
		System.out.println("表示レシピID：" + recipeId);

		// 戻り先ページ表示用
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null || request.getParameter("page_num").equals("") ? "1" : request.getParameter("page_num"));

		// レシピIDに紐づくレシピ詳細とカテゴリ名を取得
		RecipeDao recipeDao = new RecipeDao();
		RecipeBeans recipeDetail = recipeDao.getDetail(recipeId);

		// レシピIDに紐づくタグIDとタグ名を取得
		RecipeTagDao recipeTagDao = new RecipeTagDao();
		List<RecipeTagBeans> tagList = recipeTagDao.getLinkedTags(recipeId);

		// レシピ情報諸々と戻り先ページ番号をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("recipeDetail", recipeDetail);
		request.setAttribute("tagList", tagList);
		request.setAttribute("pageNum", pageNum);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeDetail.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
