package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RecipeDao;

@WebServlet("/RecipeDeleteServlet")
public class RecipeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeDeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてレシピIDを受け取って型変換
		int recipeId = Integer.parseInt(request.getParameter("recipe_id"));
		System.out.println("削除レシピID：" + recipeId);

		// 戻り先ページ表示用
		//int pageNum = Integer.parseInt(request.getParameter("page_num")==null?"1":request.getParameter("page_num"));

		// 該当レシピIDのレコードをレシピテーブルとレシピタグテーブルとお気に入りテーブルから削除
		RecipeDao recipeDao = new RecipeDao();
		recipeDao.deleteRecipe(recipeId);

		// 検索結果のサーブレットにリダイレクト
		response.sendRedirect("RecipeSearchServlet");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
