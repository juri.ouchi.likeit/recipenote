package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.CategoryDao;
import dao.RecipeDao;
import dao.RecipeTagDao;
import dao.TagDao;
import model.CategoryBeans;
import model.RecipeBeans;
import model.RecipeTagBeans;
import model.TagBeans;
import model.UserBeans;

@WebServlet("/RecipeAddServlet")
@MultipartConfig
public class RecipeAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeAddServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// ユーザーが登録しているカテゴリ名とタグ名を取得
		CategoryDao categoryDao = new CategoryDao();
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);

		TagDao tagDao = new TagDao();
		List<TagBeans> tagList = tagDao.getTagName(userId);

		// リクエストスコープに下記をセット
		// 総ページ数、表示ページ番号、レシピ一覧情報、カテゴリ名、タグ名
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("tagList", tagList);

		// レシピ追加画面のJSPにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeAdd.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String recipeName = request.getParameter("recipeName");

		// 画像ファイルがあれば、指定フォルダに取り込んでファイル名を書換・保存
		String image = "";
		Part part = request.getPart("image");
		String fileName = RecipenoteHelper.extractFileName(part);
		if (!fileName.equals("")) {
			image = "u" + userId + "_" + fileName;
			part.write(Parse.IMAGE_DESTINATION_FOLDER + "/" + image);
		}

		String description = request.getParameter("description");
		String servings = request.getParameter("servings");
		String ingredients = request.getParameter("ingredients");
		String preparation = request.getParameter("preparation");
		String tips = request.getParameter("tips");
		String source = request.getParameter("source");
		int categoryId = Integer.parseInt(request.getParameter("categorySelect"));
		String[] tagIds = request.getParameterValues("tagSelect");

		// 抽出したレシピ情報をレシピテーブルに登録
		RecipeDao recipeDao = new RecipeDao();
		int recipeId = recipeDao.saveRecipe(recipeName, image, description, servings,
				ingredients, preparation, tips, source, categoryId, userId);

		// タグ情報をレシピタグテーブルに登録
		RecipeTagDao recipeTagDao = new RecipeTagDao();
		if(tagIds != null && tagIds.length > 0) {
			recipeTagDao.addLinkingTag(recipeId, tagIds);
		}

		System.out.println("Saving recipe by form has been completed.");

		if (recipeId != 0) {
        	// レシピIDに紐づくレシピ詳細とカテゴリ名を取得、リクエストスコープにセット
    	    RecipeBeans recipeDetail = recipeDao.getDetail(recipeId);
    	    request.setAttribute("recipeDetail", recipeDetail);

    	    // レシピIDに紐づくタグIDとタグ名を取得、リクエストスコープにセット
    		List<RecipeTagBeans> tagList = recipeTagDao.getLinkedTags(recipeId);
    		request.setAttribute("tagList", tagList);

    	    // 保存したレシピの詳細画面のJSPにフォワード
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeDetail.jsp");
    	    dispatcher.forward(request, response);

		} else {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "レシピの保存に失敗しました。");

			// レシピ追加画面のJSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeParse.jsp");
    	    dispatcher.forward(request, response);
		}

	}

}
