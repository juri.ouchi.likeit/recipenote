package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CategoryDao;
import dao.RecipeDao;
import dao.RecipeTagDao;
import dao.TagDao;
import model.CategoryBeans;
import model.RecipeBeans;
import model.RecipeTagBeans;
import model.TagBeans;
import model.UserBeans;

@WebServlet("/RecipeSearchServlet")
public class RecipeSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RecipeSearchServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// ユーザーが登録しているカテゴリ名とタグ名を取得
		CategoryDao categoryDao = new CategoryDao();
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);

		TagDao tagDao = new TagDao();
		List<TagBeans> tagList = tagDao.getTagName(userId);

		// リクエストパラメータに指定されたページ番号がなければ1ページ目を表示
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null || request.getParameter("page_num").equals("") ? "1" : request.getParameter("page_num"));

		double allRecipeCount = 0.0;

		// URLに検索対象のカテゴリIDかタグIDが含まれていたら取得
		int categoryId = Integer.parseInt(request.getParameter("category_id") == null ? "0" : request.getParameter("category_id"));
		int tagId = Integer.parseInt(request.getParameter("tag_id") == null ? "0" : request.getParameter("tag_id"));

		if (categoryId > 0) {
			// セッションに検索用パーツが入っていたら破棄
			RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

			// 新たに検索されたカテゴリをセッションに格納
			session.setAttribute("searchCategory", categoryId);

			// 対象カテゴリに登録されたレシピ情報を取得、セット
			RecipeDao recipeDao = new RecipeDao();
			List<RecipeBeans> recipeList = recipeDao.findAllByCategory(categoryId, pageNum, RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);
			request.setAttribute("recipeList", recipeList);

			// 対象カテゴリに登録されたレシピ総数を取得
			allRecipeCount = recipeDao.getRecipeCountByCategory(categoryId);

		} else if (tagId > 0) {
			// セッションに検索用パーツが入っていたら破棄
			RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

			// 新たに検索されたタグをセッションに格納
			String[] tagIds = {Integer.toString(tagId)};
			session.setAttribute("searchTag", tagIds);

			// 対象タグに登録されたレシピ情報を取得、セット
			RecipeTagDao recipeTagDao = new RecipeTagDao();
			List<RecipeTagBeans> recipeList = recipeTagDao.findAllByTag(tagId, pageNum, RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);
			request.setAttribute("recipeList", recipeList);

			// 対象タグに登録されたレシピ総数を取得
			allRecipeCount = recipeTagDao.getRecipeCountByTag(tagId);

		} else {
			// セッションから検索パーツを取り出してレシピ情報を取得してセット、総数を取得
			String searchWord = (String) session.getAttribute("searchWord");
	        String searchCategory = session.getAttribute("searchCategory") == null ? null : session.getAttribute("searchCategory").toString();
 	        String[] searchTags = (String[]) session.getAttribute("searchTag");
			String[] searchFavs = (String[]) session.getAttribute("searchFav");

			// 検索対象のレシピ情報を取得、セット
	     	RecipeDao recipeDao = new RecipeDao();
	     	List<RecipeBeans> recipeList = recipeDao.searchRecipe(userId, searchWord, searchCategory, searchTags, searchFavs, pageNum, RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);
	     	request.setAttribute("recipeList", recipeList);

			// 検索対象のレシピ総数を取得
			allRecipeCount = recipeDao.getRecipeCountByForm(userId, searchWord, searchCategory, searchTags, searchFavs);
		}

		// 総ページ数を出す
		int pageMax = (int) Math.ceil(allRecipeCount / RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);
		System.out.println("レシピ総数:" + allRecipeCount + "、総ページ数：" + pageMax);

		// リクエストスコープに諸々をセット
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("pageMax", pageMax);
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("tagList", tagList);

		// 検索結果のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeSearchResult.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
        String recipeWord = request.getParameter("recipeWord");
        String categoryId = request.getParameter("categorySelect");
        String[] tagIds = request.getParameterValues("tagSelect");
		String[] favs = request.getParameterValues("favCheck");

		int pageNum = 1;

		// セッションに検索用パーツが入っていたら破棄
		RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

		// 検索に使うパーツをセッションに保存
		if(!recipeWord.equals("")) {
			session.setAttribute("searchWord", recipeWord);
        }

        if(!categoryId.equals("")) {
        	session.setAttribute("searchCategory", categoryId);
        }

        if(tagIds != null && tagIds.length > 0) {
        	session.setAttribute("searchTag", tagIds);
        }

        if(favs != null && favs.length > 0) {
        	session.setAttribute("searchFav", favs);
        }

        // レシピ情報を取得
     	RecipeDao recipeDao = new RecipeDao();
     	List<RecipeBeans> recipeList = recipeDao.searchRecipe(userId, recipeWord, categoryId, tagIds, favs, pageNum, RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);

		// 検索対象レシピの総数を取得、表示数で割って総ページ数を出す
		double allRecipeCount = recipeDao.getRecipeCountByForm(userId, recipeWord, categoryId, tagIds, favs);
		int pageMax = (int) Math.ceil(allRecipeCount / RecipenoteHelper.PAGE_MAX_RECIPE_COUNT);
		System.out.println("検索対象レシピ総数:" + allRecipeCount + "、総ページ数：" + pageMax);

		// ユーザーが登録しているカテゴリ名とタグ名を取得
		CategoryDao categoryDao = new CategoryDao();
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);
		TagDao tagDao = new TagDao();
		List<TagBeans> tagList = tagDao.getTagName(userId);

		// リクエストスコープに下記をセット
		// 総ページ数、表示ページ番号、レシピ一覧情報、カテゴリ名、タグ名
		request.setAttribute("pageMax", pageMax);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("recipeList", recipeList);
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("tagList", tagList);

		// jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeSearchResult.jsp");
		dispatcher.forward(request, response);
	}

}
