package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CategoryDao;
import dao.UserDao;
import model.UserBeans;

@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserCreateServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// エラーで戻ってきた場合、セッションから入力内容を復元・破棄してリクエストスコープにセット
		String regErrMsg = session.getAttribute("regErrMsg") == null ? null : (String) session.getAttribute("regErrMsg");

		if (regErrMsg != null) {
			String loginId = (String) session.getAttribute("loginId");
			String userName = (String) session.getAttribute("userName");
			session.removeAttribute("regErrMsg");
			session.removeAttribute("loginId");
			session.removeAttribute("userName");

			request.setAttribute("regErrMsg", regErrMsg);
			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String conPassword = request.getParameter("confirmPassword");

		// エラーメッセージを格納する変数
		String regErrMsg = null;

		// ログインIDのバリデーション
		boolean validation = RecipenoteHelper.validateLoginId(loginId);
		if (!validation) {
			regErrMsg = "ログインIDには半角英数とハイフン、アンダースコアのみ使用できます。";
		}

		// ログインIDの重複確認
		UserDao userDao = new UserDao();
		int checkUserId = userDao.checkLoginId(loginId);
		if (checkUserId != 0) {
			regErrMsg = "入力されたログインIDは既に使用されています。";
		}

		// パスワードの一致確認
		if (!password.equals(conPassword)) {
			regErrMsg = "確認用のパスワードが一致しません。";
		}

		if(regErrMsg == null){
			// エラーメッセージがなければ、新規登録
			int userId = userDao.createUser(loginId, userName, password);
			// 未分類カテゴリを作成
			CategoryDao categoryDao = new CategoryDao();
			categoryDao.addRootCategory(userId);

			// セッションにユーザーの情報をセット
			UserBeans user = new UserBeans(userId, loginId, userName);
			session.setAttribute("userInfo", user);
			// メイン画面のサーブレットにリダイレクト
			response.sendRedirect("RecipeListServlet");

		} else {
			// リクエストスコープにエラーメッセージと入力内容をセット
			session.setAttribute("regErrMsg", regErrMsg);
			session.setAttribute("loginId", loginId);
			session.setAttribute("userName", userName);

			// 新規登録JSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		}
	}

}
