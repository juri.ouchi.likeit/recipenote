package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションがある場合、TOP画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");

		if (userSession != null) {
			response.sendRedirect("RecipeListServlet");
		} else {
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		// 入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		UserBeans user = userDao.findByLoginInfo(loginId, password);

		if (user == null) {
			/** テーブルに該当のデータが見つからなかった場合 **/

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力内容が正しくありません。");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);

		} else {
			// セッションにユーザーの情報をセット
			HttpSession session = request.getSession();
			session.setAttribute("userInfo", user);

			// レシピ一覧のサーブレットにリダイレクト
			response.sendRedirect("RecipeListServlet");

		}
	}

}
