package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String userName = request.getParameter("newUserName");
		String password = request.getParameter("newPassword");
		String conPassword = request.getParameter("newConfirmPassword");

	 	if (!password.equals(conPassword)) {
			/** パスワードとパスワード(確認)が一致しない場合 **/
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "確認用のパスワードが一致しません。");

	 	} else if (password.equals("")) {
			/** パスワードが空欄の場合 **/
	 		// ユーザー名のみを更新してセッションも更新
	 		UserDao userDao = new UserDao();
	 		userDao.updateUser(userId, userName);
	 		userSession.setUserName(userName);

	 		// リクエストスコープにメッセージをセット
			request.setAttribute("infoMsg", "ユーザー名を更新しました。");

		} else {
			// ユーザー名とパスワードを更新してセッションも更新
			UserDao userDao = new UserDao();
			userDao.updateUser(userId, userName, password);
			userSession.setUserName(userName);

			// リクエストスコープにメッセージをセット
			request.setAttribute("infoMsg", "ユーザー情報を更新しました。");

		}

	 	// ユーザー詳細画面のJSPにフォワード
	 	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
	 	dispatcher.forward(request, response);
	}

}
