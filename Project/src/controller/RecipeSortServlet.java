package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CategoryDao;
import dao.RecipeDao;
import dao.TagDao;
import model.CategoryBeans;
import model.TagBeans;
import model.UserBeans;

@WebServlet("/RecipeSortServlet")
public class RecipeSortServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeSortServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// セッションに検索用パーツが入っていたら破棄
		RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

		// ユーザーが登録しているカテゴリ名とタグ名を取得、セット
		CategoryDao categoryDao = new CategoryDao();
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);
		request.setAttribute("categoryList", categoryList);

		TagDao tagDao = new TagDao();
		List<TagBeans> tagList = tagDao.getTagName(userId);
		request.setAttribute("tagList", tagList);

		// カテゴリ・タグ設定画面のJSPにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeSort.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		CategoryDao categoryDao = new CategoryDao();
		TagDao tagDao = new TagDao();

		// エラーメッセージ格納用の変数
		String sortErrMsg = null;
		// 動的追加枠の最大数
		int maxCount = 6;

		// カテゴリ、タグどちらの更新ボタンが押されたかを判断する変数
		String updateType = request.getParameter("updateButton");

		switch (updateType) {
			case "category":
				/* 削除 */
				// リクエストパラメータの削除チェック項目を取得
				String[] delCategories = request.getParameterValues("delCategory");

				if (delCategories != null && delCategories.length > 0) {
					// チェックつきIDのカテゴリに紐づいたレシピを未分類カテゴリに書き換え
					RecipeDao recipeDao = new RecipeDao();
					recipeDao.relinkToRoot(userId, delCategories);
					// チェックつきIDのカテゴリを削除
					categoryDao.deleteCategory(delCategories);
				}

				/* 更新 */
				// 更新用のリスト
				List<CategoryBeans> updateCategoryList = new ArrayList<CategoryBeans>();
				// ユーザーが登録済みのカテゴリリストを取得
				List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);

				// カテゴリIDごとの記入欄に入力された値を取得し、重複チェック
				for (int i = 0; i < categoryList.size(); i++) {
					int categoryId = categoryList.get(i).getCategoryId();
					String updateCategoryName = request.getParameter("ct" + String.valueOf(categoryId));

					int checkUpdate = categoryDao.checkCategoryName(updateCategoryName, userId);
					if (checkUpdate == 0) {
						// 重複するカテゴリ名がなければ、更新用のリストに追加
						CategoryBeans category = new CategoryBeans(categoryId, updateCategoryName);
						updateCategoryList.add(category);
					} else if (checkUpdate != 0 && sortErrMsg == null) {
						sortErrMsg = "既に登録されているカテゴリ名と重複するものは保存されませんでした。";
					}
				}

				if (updateCategoryList.size() > 0) {
					// カテゴリ名を更新
					categoryDao.updateCategory(updateCategoryList);
				}

				/*
				List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);

				for (int i = 0; i < categoryList.size(); i++) {
					String categoryId = String.valueOf(categoryList.get(i).getCategoryId());
					String updateCategoryName = request.getParameter(categoryId);
					int checkUpdate = categoryDao.checkCategoryName(updateCategoryName, userId);
					if (checkUpdate == 0) {
						// カテゴリ名を更新
						categoryDao.updateCategory(updateCategoryName, categoryId);
					}
				}
				 */

				/* 新規登録 */
				// 新規登録用のリスト
				List<String> newCategoryList = new ArrayList<String>();

				// リクエストパラメータの新規作成項目を取得
				for (int i = 1; i <= maxCount; i++) {
					String newCategoryName = request.getParameter("newCt" + i);

					if (newCategoryName != null && !newCategoryName.equals("")) {
						// 同名カテゴリが登録されていないかチェック
						int checkNew = categoryDao.checkCategoryName(newCategoryName, userId);

						if (checkNew == 0) {
							newCategoryList.add(newCategoryName);

						} else if (checkNew != 0 && sortErrMsg == null) {
							sortErrMsg = "既に登録されているカテゴリ名と重複するものは保存されませんでした。";
						}
					}
				}

				// 新しいカテゴリを登録
				if (newCategoryList.size() > 0) {
					categoryDao.addNewCategory(newCategoryList, userId);
				}

				/*
				 // リクエストパラメータの新規作成項目を取得
				String newCategoryName = request.getParameter("newCategory1");
				if (newCategoryName != "") {
					// 同名カテゴリが登録されていないかチェック
					int checkNew = categoryDao.checkCategoryName(newCategoryName, userId);
					if (checkNew == 0) {
						// 新しいカテゴリを登録
						categoryDao.addNewCategory(newCategoryName, userId);
					} else {
						// リクエストスコープにエラーメッセージをセット
						request.setAttribute("errMsg", "同名のカテゴリが既に登録されています。");
					}
				}
				 */

				break;

			case "tag":
				/* 削除 */
				// リクエストパラメータの削除チェック項目を取得
				String[] delTags = request.getParameterValues("delTag");

				if (delTags != null && delTags.length > 0) {
					// チェックつきIDのタグと関連レシピタグを削除
					tagDao.deleteTag(delTags);
				}

				/* 更新 */
				// 更新用のリスト
				List<TagBeans> updateTagList = new ArrayList<TagBeans>();
				// ユーザーが登録済みのタグリストを取得
				List<TagBeans> tagList = tagDao.getTagName(userId);

				// タグIDごとの記入欄に入力された値を取得し、重複チェック
				for (int i = 0; i < tagList.size(); i++) {
					int tagId = tagList.get(i).getTagId();
					String updateTagName = request.getParameter("tag" + String.valueOf(tagId));

					int checkUpdate = tagDao.checkTagName(updateTagName, userId);
					if (checkUpdate == 0) {
						// 重複するタグ名がなければ、更新用のリストに追加
						TagBeans tag = new TagBeans(tagId, updateTagName);
						updateTagList.add(tag);
					} else if (checkUpdate != 0 && sortErrMsg == null) {
						sortErrMsg = "既に登録されているタグ名と重複するものは保存されませんでした。";
					}
				}

				if (updateTagList.size() > 0) {
					// タグ名を更新
					tagDao.updateTag(updateTagList);
				}

				/* 新規登録 */
				// 新規登録用のリスト
				List<String> newTagList = new ArrayList<String>();

				// リクエストパラメータの新規作成項目を取得
				for (int i = 1; i <= maxCount; i++) {
					String newTagName = request.getParameter("newTag" + i);

					if (newTagName != null && !newTagName.equals("")) {

						switch (tagList.size()) {
						case 0:
							newTagList.add(newTagName);
							break;

						default:
							// 同名タグが登録されていないかチェック
							int checkNew = tagDao.checkTagName(newTagName, userId);

							if (checkNew == 0) {
								newTagList.add(newTagName);

							} else if (checkNew != 0 && sortErrMsg == null) {
								sortErrMsg = "既に登録されているタグ名と重複するものは保存されませんでした。";
							}
							break;
						}

					}
				}

				// 新しいタグを登録
				if (newTagList.size() > 0) {
					tagDao.addNewTag(newTagList, userId);
				}

				/*
				// リクエストパラメータの新規作成項目を取得
				String newTagName = request.getParameter("newTag1");
				if (newTagName != "") {
					// 同名タグが登録されていないかチェック
					int checkNew = tagDao.checkTagName(newTagName, userId);
					if (checkNew == 0) {
						// 新しいタグを登録
						tagDao.addNewTag(newTagName, userId);
					} else {
						// リクエストスコープにエラーメッセージをセット
						request.setAttribute("errMsg", "同名のタグが既に登録されています。");
					}
				}
				 */
				break;
		}

		// リクエストスコープにエラーメッセージをセット
		request.setAttribute("errMsg", sortErrMsg);

		// ユーザーが登録しているカテゴリ名とタグ名を取得しなおしてセット
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);
		request.setAttribute("categoryList", categoryList);

		List<TagBeans> tagList = tagDao.getTagName(userId);
		request.setAttribute("tagList", tagList);

		// カテゴリ・タグ設定のJSPにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeSort.jsp");
		dispatcher.forward(request, response);
	}

}
