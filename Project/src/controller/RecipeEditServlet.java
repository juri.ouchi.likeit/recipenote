package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.CategoryDao;
import dao.RecipeDao;
import dao.RecipeTagDao;
import dao.TagDao;
import model.CategoryBeans;
import model.RecipeBeans;
import model.RecipeTagBeans;
import model.TagBeans;
import model.UserBeans;

@WebServlet("/RecipeEditServlet")
@MultipartConfig
public class RecipeEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeEditServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてレシピIDを受け取って型変換
		int recipeId = Integer.parseInt(request.getParameter("recipe_id"));
		System.out.println("編集レシピID：" + recipeId);

		// 戻り先ページ表示用
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null || request.getParameter("page_num").equals("") ? "1" : request.getParameter("page_num"));


		// レシピIDに紐づくレシピ詳細とカテゴリ名を取得
		RecipeDao recipeDao = new RecipeDao();
		RecipeBeans recipeDetail = recipeDao.getDetail(recipeId);

		// レシピIDに紐づくタグIDとタグ名を取得
		RecipeTagDao recipeTagDao = new RecipeTagDao();
		List<RecipeTagBeans> linkedTagList = recipeTagDao.getLinkedTags(recipeId);

		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// ユーザーが登録しているカテゴリ名とタグ名を取得
		CategoryDao categoryDao = new CategoryDao();
		List<CategoryBeans> categoryList = categoryDao.getCategoryName(userId);

		TagDao tagDao = new TagDao();
		List<TagBeans> tagList = tagDao.getTagName(userId);

		// レシピ、カテゴリ、タグ情報と戻り先ページ番号をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("recipeDetail", recipeDetail);
		request.setAttribute("linkedTagList", linkedTagList);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("tagList", tagList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeEdit.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		int recipeId = Integer.parseInt(request.getParameter("editRecipeId"));
		System.out.println("更新レシピID：" + recipeId);

		String recipeName = request.getParameter("newRecipeName");
		String description = request.getParameter("newDescription");
		String servings = request.getParameter("newServings");
		String ingredients = request.getParameter("newIngredients");
		String preparation = request.getParameter("newPreparation");
		String tips = request.getParameter("newTips");
		String source = request.getParameter("newSource");
		int categoryId = Integer.parseInt(request.getParameter("categorySelect"));
		String[] tagIds = request.getParameterValues("tag[]");

		// 画像ファイルがあれば、指定フォルダに取り込んでファイル名を書換・保存
		Part part = request.getPart("newImage");
		String fileName = RecipenoteHelper.extractFileName(part);

		RecipeDao recipeDao = new RecipeDao();
		if (!fileName.equals("")) {
			String image = "u" + userId + "_" + fileName;
			part.write(Parse.IMAGE_DESTINATION_FOLDER + "/" + image);

			// 入力されたレシピ情報をもとにレシピテーブルを更新（imageを含む）
			recipeDao.updateRecipe(recipeId, recipeName, image, description, servings,
					ingredients, preparation, tips, source, userId, categoryId);

		} else {
			// 入力されたレシピ情報をもとにレシピテーブルを更新（imageを含まない）
			recipeDao.updateRecipe(recipeId, recipeName, description, servings,
					ingredients, preparation, tips, source, userId, categoryId);
		}

		// タグ情報をもとにレシピタグテーブルを更新
		//（まずレシピに紐づくタグを全削除し、チェックされたタグがあれば登録しなおす）
		RecipeTagDao recipeTagDao = new RecipeTagDao();
		recipeTagDao.deleteRecipeTag(recipeId);

		if(tagIds != null && tagIds.length > 0) {
			recipeTagDao.addLinkingTag(recipeId, tagIds);
		}

		System.out.println("Updating recipe has been completed.");

		// レシピIDに紐づくレシピ詳細とカテゴリ名を取得し、リクエストスコープにセット
		RecipeBeans recipeDetail = recipeDao.getDetail(recipeId);
		request.setAttribute("recipeDetail", recipeDetail);

		// レシピIDに紐づくタグIDとタグ名を取得し、リクエストスコープにセット
		List<RecipeTagBeans> tagList = recipeTagDao.getLinkedTags(recipeId);
		request.setAttribute("tagList", tagList);

		// 戻り先ページ表示用
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null || request.getParameter("page_num").equals("") ? "1" : request.getParameter("page_num"));
		request.setAttribute("pageNum", pageNum);

		// 対象レシピの詳細画面のJSPにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeDetail.jsp");
		dispatcher.forward(request, response);
	}

}
