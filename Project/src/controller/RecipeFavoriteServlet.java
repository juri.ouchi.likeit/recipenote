package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.FavoriteDao;
import dao.RecipeDao;
import dao.RecipeTagDao;
import model.RecipeBeans;
import model.RecipeTagBeans;
import model.UserBeans;

@WebServlet("/RecipeFavoriteServlet")
public class RecipeFavoriteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeFavoriteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// URLからGETパラメータとしてレシピIDを受け取って型変換
		int recipeId = Integer.parseInt(request.getParameter("recipe_id"));

		// 戻り先ページ表示用
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null || request.getParameter("page_num").equals("") ? "1" : request.getParameter("page_num"));

		// 該当レシピIDがお気に入り登録されているか確認
		FavoriteDao favoriteDao = new FavoriteDao();
		int favoriteId = favoriteDao.searchFav(recipeId);

		if (favoriteId == 0) {
			// ユーザーIDと該当レシピIDをお気に入りテーブルに登録
			favoriteDao.addFav(userId, recipeId);

		} else {
			// お気に入りを削除
			favoriteDao.deleteFav(favoriteId);
		}

		// レシピIDに紐づくレシピ詳細とカテゴリ名を取得
		RecipeDao recipeDao = new RecipeDao();
		RecipeBeans recipeDetail = recipeDao.getDetail(recipeId);

		// レシピIDに紐づくタグIDとタグ名を取得
		RecipeTagDao recipeTagDao = new RecipeTagDao();
		List<RecipeTagBeans> tagList = recipeTagDao.getLinkedTags(recipeId);

		// レシピ情報諸々と戻り先ページ番号をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("recipeDetail", recipeDetail);
		request.setAttribute("tagList", tagList);
		request.setAttribute("pageNum", pageNum);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeDetail.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
