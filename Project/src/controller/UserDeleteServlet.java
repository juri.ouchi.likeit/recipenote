package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserDeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// セッションに検索用パーツが入っていたら破棄
		RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

		// セッション内のユーザー情報を削除
		session.removeAttribute("userInfo");

		// ユーザーの全情報を削除
		UserDao userDao = new UserDao();
		userDao.deleteUser(userId);

		// リクエストスコープにメッセージをセット
		request.setAttribute("infoMsg", "退会処理が完了しました。ご利用ありがとうございました。");

		// ログイン画面のJSPにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
