package controller;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

/**
 * 処理・表示簡略化ヘルパークラス
 */

public class RecipenoteHelper {
	// 1ページに表示するレシピ数
	static final int PAGE_MAX_RECIPE_COUNT = 6;

	/**
	 * ハッシュ関数
	 *
	 * @param target
	 * @return 暗号化したパスワード
	 */
	public static String getMd5(String target) {
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(target.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	* セッションの検索用パーツを破棄
	*
	* @param
	*/
	public static void removeSessionAttribute(HttpSession session, String searchWord, String searchCategory, String searchTag, String searchFav) {
		Object wordData = session.getAttribute(searchWord);
		if(wordData != null) {
			session.removeAttribute(searchWord);
		}

		Object categoryData = session.getAttribute(searchCategory);
		if(categoryData != null) {
			session.removeAttribute(searchCategory);
		}

		Object tagData = session.getAttribute(searchTag);
		if(tagData != null) {
			session.removeAttribute(searchTag);
		}

		Object favData = session.getAttribute(searchFav);
		if(favData != null) {
			session.removeAttribute(searchFav);
		}
	}

	/**
	 * ログインIDのバリデーション
	 *
	 * @param
	 * @return boolean
	 */
	public static boolean validateLoginId(String loginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (loginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		return false;
	}

	/**
	* 料理写真のファイル名を取り出す
	*
	* @param
	* @return fileName
	*/
	public static String extractFileName(Part part) {
	  System.out.println(part.getHeader("Content-Disposition"));
	  String[] splitedHeader = part.getHeader("Content-Disposition").split(";");

	  String fileName = null;
	  for(String item: splitedHeader) {
	      if(item.trim().startsWith("filename")) {
	          fileName = item.substring(item.indexOf('"')).replaceAll("\"", "");
	      }
	  }
	  return fileName;
	}

}
