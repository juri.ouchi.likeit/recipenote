package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.RecipeDao;
import model.RecipeBeans;
import model.UserBeans;

@WebServlet("/RecipeParseServlet")
public class RecipeParseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RecipeParseServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッションに検索用パーツが入っていたら破棄
		HttpSession session = request.getSession();
		RecipenoteHelper.removeSessionAttribute(session, "searchWord", "searchCategory", "searchTag", "searchFav");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeParse.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン時に取得したユーザーIDをセッションから取得
		HttpSession session = request.getSession();
		UserBeans userSession = (UserBeans) session.getAttribute("userInfo");
		int userId = userSession.getUserId();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String url = request.getParameter("url");

		int recipeId = 0;

		// 入力されたサイトを判別しスクレイピング
        if (url.contains("https://www.kyounoryouri.jp/recipe/")) {
        	recipeId = Parse.saveKyounoryouri(url, userId);

        } else if (url.contains("https://cookpad.com/recipe/")) {
        	recipeId = Parse.saveCookpad(url, userId);

		} else if (url.contains("https://erecipe.woman.excite.co.jp/detail/")) {
			recipeId = Parse.saveERecipe(url, userId);

		} else {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "URL保存対象外サイトです。お手数ですが直接入力で保存してください。");

			// レシピ追加画面のJSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeParse.jsp");
			dispatcher.forward(request, response);
			return;
		}

        if (recipeId != 0) {
        	// レシピIDに紐づくレシピ詳細とカテゴリ名を取得、リクエストスコープにセット
    	    RecipeDao recipeDao = new RecipeDao();
    	    RecipeBeans recipeDetail = recipeDao.getDetail(recipeId);
    	    request.setAttribute("recipeDetail", recipeDetail);

    	    // 保存したレシピ詳細画面のJSPにフォワード
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeDetail.jsp");
    	    dispatcher.forward(request, response);
		} else {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "レシピの保存に失敗しました。");

			// レシピ追加画面のJSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeParse.jsp");
    	    dispatcher.forward(request, response);
		}

	}

}
