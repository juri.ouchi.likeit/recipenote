package controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import dao.RecipeDao;

public class Parse {
	// 料理写真保存先フォルダ
	static final String IMAGE_DESTINATION_FOLDER = "C:/Users/LIKEIT_STUDENT.DESKTOP-KCEOEF0.002/Documents/recipenote/Project/WebContent/image/photo";

	/**
	 * 画像URLから画像を保存
	 *
	 * @param
	 * @return fileName + "." + imageType
	 */
	public static String saveImage(int userId, String imageUrl){
        try {
        	// URLから画像をバイト化
        	BufferedImage image = ImageIO.read(
        		    new ByteArrayInputStream(
        		        Jsoup
        		        .connect(imageUrl)
        		        .ignoreContentType(true)
        		        .execute()
        		        .bodyAsBytes()
        		    )
        		);

        	// 画像URLの"/xxx.jp(e)gyyyyyy"からxxxの部分だけ取り出す
        	// 画像名にユーザーIDを付加して保存
        	String imageName = imageUrl.substring( imageUrl.lastIndexOf("/") + 1, imageUrl.lastIndexOf(".") );
        	String imageType = "jpg";
        	String fileName = "u" + userId + "_" + imageName + "." + imageType;
        	String filePath = IMAGE_DESTINATION_FOLDER + "/" + fileName;
        	ImageIO.write(image, imageType, new File(filePath));

            System.out.println("Saving image of the Recipe has been completed.");
            return fileName;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

	/**
	 * 「きょうの料理」スクレイピング
	 *
	 * @param
	 * @return recipeId
	 */
	public static int saveKyounoryouri(String url, int userId){

        try {
        	// 入力されたURLからhtmlを取得
        	Document doc = Jsoup.connect(url).get();

        	// 料理名
        	Element recipeNameData = doc.selectFirst("meta[property=taboola-og-title]");
    		String recipeName = recipeNameData.attr("content");

        	// 料理写真
        	Element imageUrlData = doc.selectFirst("meta[property=og:image]");
    		String imageUrl = imageUrlData.attr("content");
    		String image = saveImage(userId, imageUrl);

        	// 説明
    		Element descriptionData = doc.selectFirst("p.description");
    		String description = descriptionData.text();

        	// 分量
    		Element servingsData = doc.selectFirst(".detail-sub p:lt(2)");
    		String servings = servingsData.text();

    		// 材料
    		Elements ingredientsData = doc.select("#ingredients_list dt,dd");
    		StringBuilder ingSb = new StringBuilder();

            for (Element ingEl : ingredientsData) {
            	String ing = ingEl.text();
            	ingSb.append(ing);
            	if (!ing.isEmpty()) {
            		ingSb.append("\n");
    			}
            }

            String ingredients = ingSb.toString();

    		// 下ごしらえ＋作り方
            StringBuilder prepSb = new StringBuilder();

            Element prePreparationTitle = doc.selectFirst(".detail-recipe-heading:eq(3)");
            prepSb.append("　" + prePreparationTitle.text() + "\n");

            Elements prePreparationData = doc.select(".detail-sub p:gt(1)");
            for (Element prePrepEl : prePreparationData) {
            	prepSb.append("　" + prePrepEl.text() + "\n");
            }

            Elements preparationData = doc.select(".howto-sec");
            for (Element prepEl : preparationData) {
            	prepSb.append(prepEl.text() + "\n");
            }

            String preparation = prepSb.toString();

    		// ポイント
            String tips = "なし";
    		Element tipsData = doc.selectFirst(".detail-recipe-howtoother");
    		if (tipsData != null) {
    			tips = tipsData.text();
			}

    		// 出典
    		String source = url;

    		// 抽出したレシピ情報をレシピテーブルに登録
    		RecipeDao recipeDao = new RecipeDao();
    		int recipeId = recipeDao.saveRecipe(recipeName, image, description, servings,
    				ingredients, preparation, tips, source, userId);

    		System.out.println("Saving recipe by 'Kyou-no-ryouri' has been completed.");
    		return recipeId;

        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

	/**
	 * 「クックパッド」スクレイピング
	 *
	 * @param
	 * @return recipeId
	 */
	public static int saveCookpad(String url, int userId){

        try {
        	// 入力されたURLからhtmlを取得
        	Document doc = Jsoup.connect(url).get();

        	// 料理名
        	Element recipeNameData = doc.selectFirst("h1");
    		String recipeName = recipeNameData.text();

        	// 料理写真
        	Element imageUrlData = doc.selectFirst("#main-photo img");
    		String imageUrl = imageUrlData.attr("src");
    		String image = saveImage(userId, imageUrl);

        	// 説明
    		Element descriptionData = doc.selectFirst(".description_text");
    		String description = descriptionData.text();

        	// 分量
    		Element servingsData = doc.selectFirst(".yield");
    		String servings = servingsData.text();

    		// 材料
    		Elements ingredientsData = doc.select(".ingredient_row");
    		StringBuilder ingSb = new StringBuilder();

            for (Element ingEl : ingredientsData) {
            	ingSb.append(ingEl.text() + "\n");
            }

            String ingredients = ingSb.toString();

    		// 作り方
            Elements preparationData = doc.select("#steps dl");
            StringBuilder prepSb = new StringBuilder();

            for (Element prepEl : preparationData) {
            	prepSb.append(prepEl.text() + "\n");
            }

            String preparation = prepSb.toString();

    		// ポイント
    		Element tipsData = doc.selectFirst("#advice");
    		String tips = tipsData.text();

    		// 出典
    		String source = url;

    		// 抽出したレシピ情報をレシピテーブルに登録
    		RecipeDao recipeDao = new RecipeDao();
    		int recipeId = recipeDao.saveRecipe(recipeName, image, description, servings,
    				ingredients, preparation, tips, source, userId);

    		System.out.println("Saving recipe by 'Cookpad' has been completed.");
    		return recipeId;

        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

	/**
	 * 「Eレシピ」スクレイピング
	 *
	 * @param
	 * @return recipeId
	 */
	public static int saveERecipe(String url, int userId){

        try {
        	// 入力されたURLからhtmlを取得
        	Document doc = Jsoup.connect(url).get();

        	// 料理名
        	Element recipeNameData = doc.selectFirst("h1");
    		String recipeName = recipeNameData.text();

        	// 料理写真
        	Element imageUrlData = doc.selectFirst("meta[property=og:image]");
    		String imageUrl = imageUrlData.attr("content");
    		String image = saveImage(userId, imageUrl);

        	// 説明
    		Element descriptionData = doc.selectFirst(".summary");
    		String description = descriptionData.text();

        	// 分量
    		Element servingsData = doc.selectFirst(".yield");
    		String servings = servingsData.text();

    		// 材料
    		Elements ingredientsData = doc.select(".mate");
    		StringBuilder ingSb = new StringBuilder();

            for (Element ingEl : ingredientsData) {
            	ingSb.append(ingEl.text() + "\n");
            }

            String ingredients = ingSb.toString();

    		// 下準備＋作り方
            StringBuilder prepSb = new StringBuilder();

            Element prePreparationTitle = doc.selectFirst("h2.mL10");
            if (prePreparationTitle != null) {
            	prepSb.append("　" + prePreparationTitle.text());

            	if (!prePreparationTitle.text().isEmpty()) {
                	prepSb.append("\n");
    			}
            }

            Elements prePreparationData = doc.select("#howto_pre p");
            if (prePreparationData != null) {
            	for (Element prePrepEl : prePreparationData) {
                	prepSb.append("　" + prePrepEl.text());

                	if (!prePrepEl.text().isEmpty()) {
                    	prepSb.append("\n");
        			}
                }
			}

            Elements preparationData = doc.select(".withImg");
            for (Element prepEl : preparationData) {
            	prepSb.append(prepEl.text() + "\n");
            }

            String preparation = prepSb.toString();

    		// ポイント
            String tips = "なし";
    		Element tipsData = doc.selectFirst("tbody");
    		if (tipsData != null) {
    			tips = tipsData.text();
			}

    		// 出典
    		String source = url;

    		// 抽出したレシピ情報をレシピテーブルに登録
    		RecipeDao recipeDao = new RecipeDao();
    		int recipeId = recipeDao.saveRecipe(recipeName, image, description, servings,
    				ingredients, preparation, tips, source, userId);

    		System.out.println("Saving recipe by 'E-Recipe' has been completed.");
    		return recipeId;

        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

}
