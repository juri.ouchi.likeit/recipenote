package model;

import java.io.Serializable;

/**
 * t_recipe_tagテーブルのデータを格納するためのBeans
 */

public class RecipeTagBeans implements Serializable {
	private int recipeTagId;
	private int recipeId;
	private int tagId;

	private String tagName;

	private String recipeName;
	private String image;
	private String description;

	private int favoriteId;

	// レシピIDに紐づいたタグ表示用コンストラクタ
	public RecipeTagBeans(int tagId, String tagName) {
		this.tagId = tagId;
		this.tagName = tagName;
	}

	// タグIDに紐づいたレシピ表示用コンストラクタ
	public RecipeTagBeans(int recipeId, String recipeName, String image, String description, int tagId, int favoriteId) {
		this.recipeId = recipeId;
		this.recipeName = recipeName;
		this.image = image;
		this.description = description;
		this.tagId = tagId;
		this.favoriteId = favoriteId;
	}

	public int getRecipeTagId() {
		return recipeTagId;
	}
	public void setRecipeTagId(int recipeTagId) {
		this.recipeTagId = recipeTagId;
	}
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	public int getTagId() {
		return tagId;
	}
	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getFavoriteId() {
		return favoriteId;
	}
	public void setFavoriteId(int favoriteId) {
		this.favoriteId = favoriteId;
	}

}
