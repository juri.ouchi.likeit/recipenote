package model;

import java.io.Serializable;

/**
 * t_favoriteテーブルのデータを格納するためのBeans
 */

public class FavoriteBeans implements Serializable {
	private int favoriteId;
	private int userId;
	private int recipeId;

	//TODO コンストラクタ

	public int getFavoriteId() {
		return favoriteId;
	}
	public void setFavoriteId(int favoriteId) {
		this.favoriteId = favoriteId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

}
