package model;

import java.io.Serializable;

/**
 * t_tagテーブルのデータを格納するためのBeans
 */

public class TagBeans implements Serializable {
	private int tagId;
	private String tagName;
	private int userId;

	// コンストラクタ
	public TagBeans(int tagId, String tagName) {
		this.tagId = tagId;
		this.tagName = tagName;
	}

	public int getTagId() {
		return tagId;
	}
	public void setTagId(int tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

}
