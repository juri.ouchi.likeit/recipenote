package model;

import java.io.Serializable;

/**
 * t_recipeテーブルのデータを格納するためのBeans
 */

public class RecipeBeans implements Serializable {
	private int recipeId;
	private String recipeName;
	private String image;
	private String description;
	private String servings;
	private String ingredients;
	private String preparation;
	private String tips;
	private String source;
	private int userId;
	private int categoryId;

	private String categoryName;
	private int favoriteId;

	// 一覧表示用コンストラクタ
	public RecipeBeans(int recipeId, String recipeName, String image,
			String description, int favoriteId) {
		this.recipeId = recipeId;
		this.recipeName = recipeName;
		this.image = image;
		this.description = description;
		this.favoriteId = favoriteId;
	}

	// 詳細表示用コンストラクタ
	public RecipeBeans(int recipeId, String recipeName, String image, String description,
			String servings, String ingredients, String preparation, String tips,
			String source, int categoryId, String categoryName, int favoriteId) {
		this.recipeId = recipeId;
		this.recipeName = recipeName;
		this.image = image;
		this.description = description;
		this.servings = servings;
		this.ingredients = ingredients;
		this.preparation = preparation;
		this.tips = tips;
		this.source = source;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.favoriteId = favoriteId;
	}

	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getServings() {
		return servings;
	}
	public void setServings(String servings) {
		this.servings = servings;
	}
	public String getIngredients() {
		return ingredients;
	}
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	public String getPreparation() {
		return preparation;
	}
	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getFavoriteId() {
		return favoriteId;
	}
	public void setFavoriteId(int favoriteId) {
		this.favoriteId = favoriteId;
	}

	public String getReplacedServings() {
        return this.servings.replace(" ", "").replaceAll("^[(（]|[)）]$", "");
    }
	public String getReplacedIngredients() {
        return this.ingredients.replace("\r\n", "\n").replace("\r", "\n").replace("\n", "<br>");
    }
	public String getReplacedPreparation() {
        return this.preparation.replace("\r\n", "\n").replace("\r", "\n").replace("\n", "<br>");
    }

}
