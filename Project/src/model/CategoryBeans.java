package model;

import java.io.Serializable;

/**
 * t_categoryテーブルのデータを格納するためのBeans
 */

public class CategoryBeans implements Serializable {
	private int categoryId;
	private String categoryName;
	private int userId;

	// コンストラクタ
	public CategoryBeans(int categoryId, String categoryName) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}

	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

}
