package model;

import java.io.Serializable;

/**
 * t_userテーブルのデータを格納するためのBeans
 */

public class UserBeans implements Serializable {
	private int userId;
	private String loginId;
	private String userName;
	private String password;
	private String createDate;
	private String updateDate;


	// ログインセッションを保存するコンストラクタ
	public UserBeans(int userId, String loginId, String userName) {
		this.userId = userId;
		this.loginId = loginId;
		this.userName = userName;
	}


	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}