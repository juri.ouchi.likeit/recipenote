<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>新規登録</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/cover.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">My recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">

		<c:if test="${regErrMsg != null}">
			<div class="alert alert-warning mx-auto col-9 mt-3" role="alert">
				<i class="fas fa-exclamation-triangle fa-fw"></i> ${regErrMsg}
			</div>
		</c:if>

		<div class="form-area col-11 mb-4 mt-5">
			<form id="regist-form" method="post" action="UserCreateServlet" class="form-horizontal pt-5">
				<div class="row justify-content-center">
					<div class="col-12">
					<input type="text" class="form-control col-6 mx-auto" name="loginId" placeholder="ログインID（半角英数字とハイフン、アンダースコアのみ）"<c:if test="${loginId != null}"> value="${loginId}"</c:if> required autofocus>
					</div>

					<div class="col-12">
					<input type="text" class="form-control col-6 mx-auto" name="userName" placeholder="ユーザー名"<c:if test="${userName != null}"> value="${userName}"</c:if> required>
					</div>

					<div class="col-12">
					<input type="password" class="form-control col-6 mx-auto" name="password" placeholder="パスワード" required>
					</div>

					<div class="col-12">
					<input type="password" class="form-control col-6 mx-auto" name="confirmPassword" placeholder="パスワード（確認）" required>
					</div>
				</div>

				<div class="row justify-content-around">

					<button type="button" class="fa-stack cancel" onclick="location.href='LoginServlet'">
						<i class="fas fa-comment fa-stack-2x comment-left"></i>
						<span class="fa-stack-1x text-nowrap text-left">キャンセル</span>
					</button>
					<div class="circle"></div>
					<div class="mdi mdi-scale"></div>
					<!-- <div class="mdi mdi-scale scale-bottom"></div> -->
					<button type="submit" class="fa-stack regist">
						<i class="fas fa-comment fa-stack-2x comment-right"></i>
						<span class="fa-stack-1x text-nowrap text-right">登録</span>
					</button>
				</div>
			</form>
		</div>

        </div>

        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserCreateServlet" id="user"><i class="fas fa-user-plus"></i></a>
                <!-- <a href="login.html" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="login.html" id="add"><i class="fas fa-pen"></i></a>
                <a href="#" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>