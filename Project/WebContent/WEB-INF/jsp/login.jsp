<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ログイン</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Baloo+Tamma|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/cover.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">My recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">

        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">

            <p class="display-3 text-center logo mt-4"><span class="mdi mdi-chef-hat"></span><br>My recipe note</p>

			<c:if test="${errMsg != null}">
			    <div class="alert alert-warning mx-auto col-9 mt-3" role="alert">
				  <i class="fas fa-exclamation-triangle fa-fw"></i> ${errMsg}
				</div>
			</c:if>

			<c:if test="${infoMsg != null}">
			    <div class="alert alert-primary mx-auto col-9 mt-3" role="alert">
				  <i class="fas fa-info-circle fa-fw"></i> ${infoMsg}
				</div>
			</c:if>

				<div class="login">
					<div class="form-bg d-flex justify-content-center">
						<i class="fas fa-cloud cloud-top faa-float"><i class="fas fa-cloud cloud-bottom fa-flip-vertical"></i></i>

						<form id="login-form" method="post" action="LoginServlet">
							<div class="cp_group" id="id">
								<input type="text" id="loginId" name="loginId" required autofocus>
								<label class="cp_label" for="input">ログインID</label>
								<i class="bar"></i>
							</div>
							<div class="cp_group" id="pass">
								<input type="password" id="password" name="password" required>
								<label class="cp_label" for="input">パスワード</label>
								<i class="bar"></i>
							</div>
						</form>

					</div>

					<div class="row">
						<div class="pot col-3 d-flex justify-content-between">
							<div class="mdi mdi-pot-mix">
								<div class="fire faa-pulse">
									<i class="fas fa-burn"></i>
									<i class="fas fa-burn"></i>
									<i class="fas fa-burn"></i>
								</div>
							</div>
							<i class="fas fa-cloud cloud-top-sm faa-float"><i class="fas fa-cloud cloud-bottom-sm fa-flip-vertical"></i></i>
						</div>
						<div class="mitten col-9 d-flex justify-content-center">
							<button class="fa-stack" type="submit" form="login-form">
								<i class="fas fa-mitten"></i>
							</button>
						</div>
					</div>

				</div>

			</div>

        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserCreateServlet" id="user"><i class="fas fa-user-plus"></i></a>
                <!-- <a href="login.html" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="login.html" id="add"><i class="fas fa-pen"></i></a>
                <a href="#" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>