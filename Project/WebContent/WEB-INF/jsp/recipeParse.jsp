<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>レシピ保存</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">

			<c:if test="${errMsg != null}">
			    <div class="alert alert-danger mx-auto col-9 mt-3" role="alert">
					<i class="fas fa-exclamation-triangle fa-fw"></i> ${errMsg}
				</div>
			</c:if>

            <!-- URLフォーム -->
            <div class="form-area p-2 mb-4 mt-5">

            <form method="post" action="RecipeParseServlet" class="form-inline">
            <div class="form-group form-row col-11 pl-0">
                <label class="col-3 col-form-label px-0">レシピサイトURL</label>
                <input type="text" class="form-control col-9" id="url" name="url" placeholder="http://www.recipe-site.com/recipe-id/" autofocus>
            </div>

            <button type="submit" class="btn btn-info form-submit mx-1">保存</button>

            </form>
            </div>

            <p><i class="fas fa-utensils fa-fw fa-lg"></i>横断検索で見つけたレシピは、URLを入力するだけでスピード保存◎</p>

            <!-- Googleカスタム検索 -->
            <div class="form-area p-2 mb-4 mt-5">
            <label class="col-3 col-form-label px-0">レシピサイト横断検索</label>
            <script async src="https://cse.google.com/cse.js?cx=009871027496623029703:bmtcqhjtdeu"></script>
<div class="gcse-search"></div>
            </div>

            <p><i class="fas fa-utensils fa-fw fa-lg"></i>レシピの直接入力は<a href="RecipeAddServlet">こちら</a></p>
        </div>


        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>


    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Original JavaScript -->
    <script type="text/javascript" src="./js/common.js"></script>
</body>
</html>