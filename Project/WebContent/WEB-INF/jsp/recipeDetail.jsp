<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>レシピ詳細</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">
            <div class="row mt-4 mb-5">
                <div class="col-9">
                    <h3><span class="crayon">${recipeDetail.recipeName}</span></h3>
                </div>
                <div class="col">
                	<!-- 戻るボタン -->
                	<button type="button" class="btn btn-primary rounded-pill p-0 mr-3" style="width:3.2rem; height:2.2rem;" onclick="location.href='<c:choose><c:when test="${searchWord == null && searchCategory == null && searchTag == null && searchFav == null}">RecipeListServlet?page_num=${pageNum}</c:when><c:otherwise>RecipeSearchServlet?page_num=${pageNum}</c:otherwise></c:choose>'">
                		<i class="fas fa-long-arrow-alt-left fa-2x"></i></button>

                    <!-- お気に入りボタン -->
                    <a class="button button-toggle fa-stack mr-3<c:choose><c:when test="${recipeDetail.favoriteId != 0}"> toggle-on</c:when><c:otherwise> toggle-off</c:otherwise></c:choose>" id="toggle" href="RecipeFavoriteServlet?recipe_id=${recipeDetail.recipeId}&page_num=${pageNum}">
                        <!-- ベタ -->
                        <i class="fas fa-heart fa-stack-2x onHeart"></i>
                        <!-- 輪郭 -->
                        <i class="far fa-heart fa-stack-2x offHeart"></i>
                    </a>

                    <!-- 編集ボタン -->
                    <button type="button" class="btn btn-success rounded-circle p-0 mr-3" onclick="location.href='RecipeEditServlet?recipe_id=${recipeDetail.recipeId}&page_num=${pageNum}'"><i class="fas fa-sync-alt fa-lg"></i></button>

                    <!-- 削除ボタン -->
                    <button type="button" class="btn btn-info rounded-circle p-0" data-toggle="modal" data-target="#recipeDeleteModal"><i class="far fa-trash-alt fa-lg"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="col-5 pr-0">
                    <div class="col-12 photoFlame">
                        <div class="polaroid">
                        <span class="masking-tape"></span>
                        <img class="d-block w-100" src="./image/photo/${recipeDetail.image}" alt="">
                        </div>
                    </div>
                    <div class="col-12 pr-0 pt-3">
                        <p class="noteLine">${recipeDetail.description}</p>
                    </div>
                </div>
                <div class="col-7 pl-0 d-flex justify-content-center">
                    <h5><span class="crayon">材料<br>（${recipeDetail.replacedServings}）</span></h5>
                    <p class="noteLine">${recipeDetail.replacedIngredients}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-3">
                    <h5><span class="crayon">作り方</span></h5>
                    <p class="noteLine">${recipeDetail.replacedPreparation}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-3">
                    <h5><span class="crayon">ポイント</span></h5>
                    <p class="noteLine">${recipeDetail.tips}</p>
                    <p>出典：
                    <c:choose>
					<c:when test="${fn:contains(recipeDetail.source, 'http')}"><a href="${recipeDetail.source}" id="source">${recipeDetail.source}</a></c:when>
					<c:otherwise>${recipeDetail.source}</c:otherwise>
					</c:choose>
                    </p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <p>カテゴリ： <a href="RecipeSearchServlet?category_id=${recipeDetail.categoryId}"><i class="fas fa-folder fa-fw"></i>${recipeDetail.categoryName}</a></p>
                    <p>　　タグ：
						<c:choose>
						<c:when test="${fn:length(tagList) > 0}">
							<c:forEach var="tag" items="${tagList}">
							<a href="RecipeSearchServlet?tag_id=${tag.tagId}"><i class="fas fa-tag fa-fw"></i>${tag.tagName}</a>　
							</c:forEach>
						</c:when>
						<c:otherwise>登録なし</c:otherwise>
						</c:choose>
                    </p>
                </div>
            </div>
        </div>

        <!-- モーダル -->
        <div class="modal fade" id="recipeDeleteModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel">削除確認</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label>レシピ情報を復元することはできません。<br>本当に削除してよろしいですか？</label>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-info" data-dismiss="modal">キャンセル</button>
                        <button type="button" class="btn btn-danger form-submit" onclick="location.href='RecipeDeleteServlet?recipe_id=${recipeDetail.recipeId}&page_num=${pageNum}'">削除</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Original JavaScript -->
    <script type="text/javascript" src="./js/common.js"></script>
</body>
</html>