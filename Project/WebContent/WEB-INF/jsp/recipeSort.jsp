<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>カテゴリ・タグ管理</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">

        <c:if test="${errMsg != null}">
			<div class="alert alert-danger mx-auto col-9 mt-3" role="alert">
				<i class="fas fa-exclamation-triangle fa-fw"></i> ${errMsg}
			</div>
		</c:if>

            <div class="row">
                <form method="post" action="RecipeSortServlet" class="form-inline mt-3">
                <div class="row col-12 ml-3">
                    <h5 class="m-1"><span class="">カテゴリ</span></h5>
                    <button type="submit" class="btn btn-sm btn-danger rounded-pill form-submit mx-3" name="updateButton" value="category">更新</button>
                </div>

				<div class="row col-12">
				<c:forEach var="category" items="${categoryList}" step="1" varStatus="categoryStatus">
					<c:choose>
					<c:when test="${category.categoryName.equals('未分類')}">
					<div class="col-4 my-2">
	                    <div class="form-group form-row col-12">
	                        <label class="col-2 col-form-label"><i class="fas fa-folder fa-fw fa-2x"></i></label>
	                        <input type="text" readonly class="form-control form-control-sm" id="ct${category.categoryId}" name="ct${category.categoryId}" value="${category.categoryName}">
	                    </div>
                	</div>
					</c:when>
					<c:otherwise>
					<div class="col-4 my-2">
	                    <div class="form-group form-row col-12">
	                        <label class="col-2 col-form-label"><i class="fas fa-folder fa-fw fa-2x"></i></label>
	                        <input type="text" class="form-control form-control-sm" id="ct${category.categoryId}" name="ct${category.categoryId}" value="${category.categoryName}" required>
	                        <input class="form-check-input ml-1" type="checkbox" name="delCategory" value="${category.categoryId}" id="ctDel${category.categoryId}">
	                        <label class="form-check-label" for="ctDel${category.categoryId}">
	                            <i class="far fa-trash-alt delCheck"></i>
	                        </label>
	                    </div>
                	</div>
					</c:otherwise>
					</c:choose>
                </c:forEach>
                </div>

				<div class="row col-12" id="ct-new-area">
	                <div class="unit col-4 my-2">
	                    <div class="form-group form-row col-12">
	                        <label for="" class="col-2 col-form-label"><i class="fas fa-folder fa-fw fa-2x"></i></label>
	                        <input type="text" class="form-control form-control-sm" value="" placeholder="（新規作成）">
	                        <div class="ct-minus input-group-append">
	                        	<span class="btn btn-sm btn-outline-info">－</span>
	                        </div>
	                    </div>
	                </div>
				</div>

				<div class="form-group form-row col-12 ml-4">
					<div id="ct-plus" class="btn btn-sm btn-primary">＋</div>
				</div>
                </form>
            </div>

            <hr>

            <div class="row">
                <form method="post" action="RecipeSortServlet" class="form-inline mt-3">
                <div class="row col-12 ml-3 mb-1">
                    <h5 class="m-1"><span class="">タグ</span></h5>
                    <button type="submit" class="btn btn-sm btn-danger rounded-pill form-submit mx-3" name="updateButton" value="tag">更新</button>
                </div>

				<div class="row col-12">
                <c:forEach var="tag" items="${tagList}" step="1" varStatus="tagStatus">
                <div class="col-4 my-2">
                    <div class="form-group form-row col-12">
                        <label class="col-2 col-form-label"><i class="fas fa-tag fa-lg"></i></label>
                        <input type="text" class="form-control form-control-sm" id="tag${tag.tagId}" name="tag${tag.tagId}" value="${tag.tagName}" required>
                        <input class="form-check-input ml-1" type="checkbox" name="delTag" value="${tag.tagId}" id="tagDel${tag.tagId}">
                        <label class="form-check-label" for="tagDel${tag.tagId}">
                        <i class="far fa-trash-alt delCheck"></i>
                        </label>
                    </div>
                </div>
                </c:forEach>
                </div>

				<div class="row col-12" id="tag-new-area">
	                <div class="unit col-4 my-2">
	                    <div class="form-group form-row col-12">
	                        <label class="col-2 col-form-label"><i class="fas fa-tag fa-lg"></i></label>
	                        <input type="text" class="form-control form-control-sm" placeholder="（新規作成）">
	                        <div class="tag-minus input-group-append">
	                        	<span class="btn btn-sm btn-outline-info">－</span>
	                        </div>
	                    </div>
	                </div>
                </div>

				<div class="form-group form-row col-12 ml-4">
                	<div id="tag-plus" class="btn btn-sm btn-primary">＋</div>
                </div>
                </form>
            </div>


        </div>


        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- Original JavaScript -->
    <script type="text/javascript" src="./js/common.js"></script>
</body>
</html>