<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TOP</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
    <!-- メインカラム -->
        <!-- 検索フォーム -->
        <div class="col-11 py-3" id="main-col">
            <div class="form-area p-2 my-4">
            <form method="post" action="RecipeSearchServlet" class="form-horizontal">

            <div class="form-group form-row">
                <label class="col-2 col-form-label">料理名</label>
                <input type="text" class="form-control col-9" id="recipeWord" name="recipeWord"<c:if test="${searchWord != null}"> value="${searchWord}"</c:if> autofocus>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">カテゴリ</label>
                <select class="custom-select col-9" id="categorySelect" name="categorySelect">
                    <option value="">選択...</option>
                    <c:forEach var="category" items="${categoryList}" step="1" varStatus="categoryStatus">
						<option<c:if test="${searchCategory != null && category.categoryId == searchCategory}"> selected</c:if> value="${category.categoryId}">${category.categoryName}</option>
					</c:forEach>
                </select>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">タグ</label>
                <div class="form-row col-10 overflow-auto" style="height:3rem">
            		<!-- 検索されているタグを選択状態にする -->
                	<c:choose>
					<c:when test="${searchTag != null}">
						<c:forEach var="tag" items="${tagList}">
							<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" class="custom-control-input" name="tagSelect" value="${tag.tagId}" id="check${tag.tagId}"<c:forEach var="searchTag" items="${searchTag}"><c:if test="${searchTag == tag.tagId}"> checked</c:if></c:forEach>>
							<label class="custom-control-label" for="check${tag.tagId}">${tag.tagName}</label>
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:forEach var="tagAll" items="${tagList}">
							<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" class="custom-control-input" name="tagSelect" value="${tagAll.tagId}" id="check${tagAll.tagId}">
							<label class="custom-control-label" for="check${tagAll.tagId}">${tagAll.tagName}</label>
							</div>
						</c:forEach>
					</c:otherwise>
					</c:choose>
                </div>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">お気に入り</label>
                <div class="custom-control custom-checkbox col-8 pl-4">
                  <input type="checkbox" class="custom-control-input" name="favCheck" id="fav" value="fav"<c:if test="${searchFav != null}"> checked</c:if>>
                  <label class="custom-control-label" for="fav"><i class="fas fa-heart"></i> 登録済み</label>
                </div>

                <div class="col-2 pl-5">
                <button type="submit" class="btn btn-info form-submit">検索</button>
                </div>
            </div>
            </form>
            </div>

			<c:if test="${pageMax < 1 }"><p class="text-center">保存されているレシピはありません。</p></c:if>

            <!-- 一覧表示 -->
            <div class="row pt-2">
	            <c:forEach var="recipe" items="${recipeList}">
	            <div class="col-md-12 col-lg-6 pb-5 px-2">
	                <div class="card border border-0">
	                  <div class="row">
	                    <div class="col-sm-4 pl-2 pr-0">
	                      <img class="d-block w-100" src="./image/photo/${recipe.image}" alt="">
	                    </div>
	                    <div class="col-sm-8 p-0">
	                      <div class="card-body pt-0 pr-0">
	                          <p class="text-right m-0"><c:choose><c:when test="${recipe.favoriteId != 0}"><i class="fas fa-heart fa-lg like"></i></c:when><c:otherwise><i class="far fa-heart fa-lg offHeart"></i></c:otherwise></c:choose></p>
	                          <h5><a href="RecipeDetailServlet?recipe_id=${recipe.recipeId}&page_num=${pageNum}" class="card-title stretched-link">${recipe.recipeName}</a></h5>
	                        <p class="card-text">${recipe.description}</p>
	                      </div>
	                    </div>
	                  </div>
	                </div>
	            </div>
	            </c:forEach>
            </div>

            <!-- ページ送り -->
            <nav aria-label="result">
              <ul class="pagination justify-content-center">

              <!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="page-item disabled">
		                  <a class="page-link" tabindex="-1" aria-label="前へ">
		                    <span aria-hidden="true">&laquo;</span>
		                  </a>
		                </li>
					</c:when>
					<c:otherwise>
						<li class="page-item">
		                  <a class="page-link" href="RecipeSearchServlet?page_num=${pageNum - 1}" aria-label="前へ">
		                    <span aria-hidden="true">&laquo;</span>
		                  </a>
		                </li>
					</c:otherwise>
				</c:choose>

              <!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li class="page-item<c:if test="${pageNum == status.index }"> active </c:if>"><a class="page-link" href="RecipeSearchServlet?page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

               <!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="page-item disabled">
	                  <a class="page-link" tabindex="-1" aria-label="次へ">
	                    <span aria-hidden="true">&raquo;</span>
	                  </a>
	                </li>
				</c:when>
				<c:otherwise>
					<li class="page-item">
	                  <a class="page-link" href="RecipeSearchServlet?page_num=${pageNum + 1}" aria-label="次へ">
	                    <span aria-hidden="true">&raquo;</span>
	                  </a>
	                </li>
				</c:otherwise>
				</c:choose>

              </ul>
            </nav>

        </div>

        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>