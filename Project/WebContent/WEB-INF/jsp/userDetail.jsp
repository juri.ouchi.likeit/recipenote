<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>アカウント詳細</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger mx-auto col-9 mt-3" role="alert">
					<i class="fas fa-exclamation-triangle fa-fw"></i> ${errMsg}
				</div>
			</c:if>

			<c:if test="${infoMsg != null}">
			    <div class="alert alert-success mx-auto col-9 mt-3" role="alert">
				  <i class="fas fa-info-circle fa-fw"></i> ${infoMsg}
				</div>
			</c:if>

            <div class="form-area p-2 mb-4 mt-5">
            <form method="post" action="UserUpdateServlet" class="form-horizontal">

            <div class="form-group form-row">
                <label class="col-2 col-form-label">ログインID</label>
                <input type="text" readonly class="form-control-plaintext col-9" name="loginId" value="${userInfo.loginId}">
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">ユーザー名</label>
                <input type="text" class="form-control col-9" name="newUserName" value="${userInfo.userName}" required autofocus>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">パスワード</label>
                <input type="password" class="form-control col-9" name="newPassword" value="">
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">パスワード（確認）</label>
                <input type="password" class="form-control col-9" name="newConfirmPassword" value="">
            </div>

            <div class="row justify-content-around">
            <button type="button" class="btn btn-outline-info col-3" data-toggle="modal" data-target="#deleteModal">退会</button>
            <button type="submit" class="btn btn-info form-submit col-3 ml-5">更新</button>
            </div>

            </form>
            </div>
        </div>

        <!-- モーダル -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel">退会確認</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label>アカウントを削除すると保存したレシピ等の情報は全て失われ、<br>復元することはできません。<br>本当に退会してよろしいですか？</label>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-info" data-dismiss="modal">キャンセル</button>
                        <button type="button" class="btn btn-danger" onclick="location.href='UserDeleteServlet'">退会</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>