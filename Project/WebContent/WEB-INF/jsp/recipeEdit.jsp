<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>レシピ編集</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">
            <form method="post" action="RecipeEditServlet" class="form-horizontal" enctype="multipart/form-data">

            <div class="row mt-4 mb-5">
                <div class="form-group col-10 mx-3">
                    <input type="text" class="form-control form-control-lg" name="newRecipeName" value="${recipeDetail.recipeName}" placeholder="料理名" required autofocus>
                </div>

                <!-- 戻るボタン -->
                <button type="button" class="btn btn-info rounded-circle m-2 p-1" onclick="location.href='RecipeDetailServlet?recipe_id=${recipeDetail.recipeId}&page_num=${pageNum}'"><i class="fas fa-times fa-lg"></i></button>

                <!-- 更新ボタン -->
                <button type="submit" class="btn btn-success form-submit rounded-circle m-2 p-1"><i class="fas fa-check fa-lg"></i></button>
            </div>

            <div class="form-row">
                <div class="col-5 pr-0">
                    <div class="col-12 photoFlame">
                        <div class="polaroid">
                        <span class="masking-tape"></span>
                        <img id="dish" class="d-block w-100" src="./image/photo/${recipeDetail.image}" alt="">
                        </div>
                    <input type="file" accept="image/*" name="newImage" onchange="readURL(this);">
                    </div>

                    <div class="form-group col-12 pr-0 pt-3">
                        <input type="text" class="form-control col-12" name="newDescription" placeholder="説明" value="${recipeDetail.description}">
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-center pl-3">
                    <div><h5>材料</h5>
                    <div class="form-group col-10 p-0">
                    （<input type="text" class="form-control" name="newServings" value="${recipeDetail.servings}" placeholder="分量" required>）
                    </div>
                    <div class="form-group col-10 p-0">
                    <input type="hidden" class="form-control" name="editRecipeId" value="${recipeDetail.recipeId}">
                    </div>
                    <div class="form-group col-10 p-0">
                    <input type="hidden" class="form-control" name="page_num" value="${pageNum}">
                    </div>
					</div>
                    <div class="form-group col-8 pl-0">
                        <textarea class="form-control" name="newIngredients" placeholder="材料" rows="19" required>${recipeDetail.ingredients}</textarea>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-12 mt-3">
                    <h5>作り方</h5>
                    <div class="form-group col-12">
                        <textarea class="form-control" name="newPreparation" placeholder="作り方" rows="11" required>${recipeDetail.preparation}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12 mt-3">
                    <h5>ポイント</h5>
                    <div class="form-group col-12">
                        <input type="text" class="form-control form-control col-12" name="newTips" value="${recipeDetail.tips}">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group form-inline col-12">
                    <label>出典：</label>
                    <input type="text" class="form-control form-control-sm col-11" name="newSource" value="${recipeDetail.source}" placeholder="出典">
                </div>
            </div>
            <hr>
            <div class="form-row">
                <div class="form-inline col-12">
                    <label>カテゴリ：</label>
                    <select class="custom-select custom-select-sm col-4" id="categorySelect" name="categorySelect" required>
                    <option value="">選択...</option>
                    <!-- 登録されているカテゴリを選択状態にする -->
                    <c:forEach var="category" items="${categoryList}" step="1" varStatus="categoryStatus">
					<c:choose>
						<c:when test="${category.categoryId == recipeDetail.categoryId}">
						<option selected value="${category.categoryId}">${category.categoryName}</option>
						</c:when>
						<c:otherwise>
						<option value="${category.categoryId}">${category.categoryName}</option>
						</c:otherwise>
					</c:choose>
					</c:forEach>
                	</select>
					<!--
                    <input type="text" class="form-control form-control-sm col-4 ml-5" id="newCategory" name="newCategory" placeholder="新規カテゴリ名">
                    <input type="button" class="btn btn-primary btn-sm" id ="categoryAdd" value="追加">
                    -->
                </div>
            </div>

            <div class="form-row mt-3">
                <div class="tag col-12">
                <label>タグ：</label>
                <!-- 登録されているタグを選択状態にする -->
                	<c:choose>
					<c:when test="${fn:length(linkedTagList) > 0}">
						<c:forEach var="tag" items="${tagList}" varStatus="tagStatus">
							<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" class="custom-control-input" name="tag[]" value="${tag.tagId}" id="check${tag.tagId}"<c:forEach var="linkedTag" items="${linkedTagList}" varStatus="linkedTagStatus"><c:if test="${linkedTag.tagId == tag.tagId}"> checked</c:if></c:forEach>>
							<label class="custom-control-label" for="check${tag.tagId}">${tag.tagName}</label>
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:forEach var="tagAll" items="${tagList}" varStatus="tagStatus">
							<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" class="custom-control-input" name="tag[]" value="${tagAll.tagId}" id="check${tagAll.tagId}">
							<label class="custom-control-label" for="check${tagAll.tagId}">${tagAll.tagName}</label>
							</div>
						</c:forEach>
					</c:otherwise>
					</c:choose>
                </div>
				<!--
                <input type="text" class="form-control form-control-sm col-4 ml-5" id="newTag" name="newTag" placeholder="新規タグ名">
                <input type="button" class="btn btn-primary btn-sm" id ="tagAdd" value="追加">
                -->
            </div>

        </form>
        </div>


        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Original JavaScript -->
    <script type="text/javascript" src="./js/common.js"></script>
</body>
</html>