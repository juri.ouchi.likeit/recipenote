<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>レシピ保存</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam|Kosugi+Maru&display=swap&subset=japanese" rel="stylesheet">

    <!-- original CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/common.css" rel="stylesheet">
</head>
<body>

    <div class="container-fluid">
    <!-- 上インデックス -->
    <div class="row" id="head-row">
        <div class="col-12" id="head-col">
            <div class="header">
                <div id="headnav">
                <a href="RecipeListServlet" id="home">${userInfo.userName}'s recipe note</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="main-row">
        <!-- メインカラム -->
        <div class="col-11 py-3" id="main-col">

        	<c:if test="${errMsg != null}">
			    <div class="alert alert-danger mx-auto col-9 mt-3" role="alert">
					<i class="fas fa-exclamation-triangle fa-fw"></i> ${errMsg}
				</div>
			</c:if>

            <!-- 直接入力フォーム -->
            <div class="form-area p-2 mb-4 mt-5">
            <form method="post" action="RecipeAddServlet" class="form-horizontal" enctype="multipart/form-data">

            <div class="form-group form-row">
                <label class="col-2 col-form-label">料理名 *</label>
                <input type="text" class="form-control col-9" id="recipeName" name="recipeName" required autofocus>
            </div>

            <div class="input-group form-row mb-3">
                <label class="col-2 col-form-label mr-1">料理写真</label>
                <div class="custom-file col-8">
                    <input type="file" accept="image/*" class="custom-file-input" id="customFile" name="image">
                    <label class="custom-file-label" for="customFile" data-browse="参照">ファイル選択...</label>
                </div>
                <div class="input-group-append">
                    <button type="button" class="btn btn-outline-secondary reset">取消</button>
                </div>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">説明</label>
                <input type="text" class="form-control col-9" id="description" name="description">
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">分量 *</label>
                <input type="text" class="form-control col-9" id="servings" name="servings" placeholder="〇人前／〇cm型△個分など" required>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">材料 *</label>
                <textarea class="form-control col-9" id="ingredients" name="ingredients" rows="3" required></textarea>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">作り方 *</label>
                <textarea class="form-control col-9" id="preparation" name="preparation" rows="5" required></textarea>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">ポイント</label>
                <input type="text" class="form-control col-9" id="tips" name="tips" placeholder="注意点、コツなど">
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">出典</label>
                <input type="text" class="form-control col-9" id="source" name="source" placeholder="レシピ本のタイトル、サイトURLなど">
            </div>

             <div class="form-group form-row">
                <label class="col-2 col-form-label">カテゴリ *</label>

                <select class="custom-select col-9" id="categorySelect" name="categorySelect" required>
                    <option value="">選択...</option>
                    <c:forEach var="category" items="${categoryList}" step="1" varStatus="categoryStatus">
						<option value="${category.categoryId}">${category.categoryName}</option>
					</c:forEach>
                </select>
            </div>

            <div class="form-group form-row">
                <label class="col-2 col-form-label">タグ</label>
                <div class="form-row col-10 overflow-auto" style="height:3rem">

                <c:forEach var="tag" items="${tagList}" step="1" varStatus="tagStatus">
					<div class="custom-control custom-checkbox custom-control-inline">
	                <input type="checkbox" class="custom-control-input" name="tagSelect" value="${tag.tagId}" id="check${tag.tagId}">
	                <label class="custom-control-label" for="check${tag.tagId}">${tag.tagName}</label>
	                </div>
				</c:forEach>

                </div>
            </div>

            <div class="row justify-content-around">
            	<button type="submit" class="btn btn-info form-submit col-3">保存</button>
            </div>

            </form>
            </div>
        </div>


        <!-- 右インデックス -->
        <div class="col-1" id="right-col">
            <div class="navi">
            <div id="sidenav" class="sidenav">
                <a href="UserDetailServlet" id="user"><i class="fas fa-user"></i></a>
                <a href="RecipeSortServlet" id="settings"><i class="fas fa-folder-open"></i></a>
                <a href="RecipeParseServlet" id="add"><i class="fas fa-pen"></i></a>
                <a href="LogoutServlet" id="logout"><i class="fas fa-sign-out-alt"></i></a>
                <!-- <a href="#" id="about"><i class="far fa-calendar-alt"></i></a> -->
            </div>
            </div>
        </div>

    </div>
  </div>


    <!-- Optional JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Original JavaScript -->
    <script type="text/javascript" src="./js/common.js"></script>
</body>
</html>