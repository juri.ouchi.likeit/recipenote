CREATE DATABASE recipe_note DEFAULT CHARACTER SET utf8;

USE recipe_note;

create table t_user (
  user_id SERIAL, 
  login_id varchar(256) unique not null, 
  user_name varchar(256) not null, 
  password varchar(256) not null, 
  create_date DATETIME not null, 
  update_date DATETIME not null
);

create table t_recipe (
  recipe_id SERIAL, 
  recipe_name varchar(256) not null, 
  image varchar(256), 
  description varchar(256), 
  servings varchar(256) not null, 
  ingredients varchar(256) not null, 
  preparation varchar(256) not null, 
  tips varchar(256), 
  source varchar(256), 
  user_id int not null, 
  category_id int not null
);

create table t_favorite (
  favorite_id SERIAL, 
  user_id int not null, 
  recipe_id int not null
);

create table t_category (
  category_id SERIAL, 
  category_name varchar(256) not null
);

create table t_tag (
  tag_id SERIAL, 
  tag_name varchar(256) not null
);

create table t_recipe_tag (
  recipe_tag_id SERIAL, 
  recipe_id int not null, 
  tag_id int not null
);

INSERT INTO t_user (
	login_id, 
	user_name, 
	password, 
	create_date, 
	update_date
) VALUES (
	'rin_0223', 
	'Rin', 
	'tsk', 
	NOW(), 
	NOW()
);
