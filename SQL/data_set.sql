﻿SELECT * FROM recipe_note.t_recipe;

ALTER TABLE recipe_note.t_recipe MODIFY preparation text NOT NULL;

DESC recipe_note.t_recipe;

INSERT INTO recipe_note.t_user (
	login_id, 
	user_name, 
	password, 
	create_date, 
	update_date
) VALUES (
	'sakura_0302', 
	'Sakura', 
	'mt', 
	NOW(), 
	NOW()
);

INSERT INTO recipe_note.t_user (
	login_id, 
	user_name, 
	password, 
	create_date, 
	update_date
) VALUES (
	'shiro', 
	'Emiya', 
	'ubw', 
	NOW(), 
	NOW()
);

UPDATE recipe_note.t_user SET password = REPLACE (password, password, MD5(password)) WHERE user_id = 2 OR user_id = 3;

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'いもドーナツ', 
	'0072.jpg', 
	'特産の男爵いもを作って、農業祭や地域のお祭りにて“フライドポテト”“揚げいも”や“いもだんご”等と共に出しています。女性部（倶知安）のひとつの味です。', 
	'？個分', 
	'じゃがいも（馬鈴薯）　ＬＭ３コ\nホットケーキミックス　１５０ｇ\n砂糖　お好み\n塩　少々\nグラニュー糖　適量\n揚げ油　適量', 
	'(1)　じゃがいもは皮をむいてゆで、水を切って、よく練る。\n(2)　(1)にホットケーキミックス、砂糖、塩を入れ混ぜ、成形する。\n(3)　(2)を１７０℃に熱した油で揚げ、好みでグラニュー糖をまぶす。', 
	'★アレンジでごまやチーズを入れても良いです。', 
	'https://www.kyounoryouri.jp/recipe/10065_%E3%81%84%E3%82%82%E3%83%89%E3%83%BC%E3%83%8A%E3%83%84.html', 
	1, 
	3
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'白菜とピーマンのチャーハン', 
	'', 
	'パパっと中華な味＊',
	'1人分', 
	'白菜4分の1を3枚\nピーマン1個\nご飯１人分\n卵2個\nコンソメ1個\n塩こしょう少々\n■ 油', 
	'1　白菜、ピーマンを約1㎝角に切る。小さめでもいいです。\n2　フライパンに油をしいて【1】を炒める。コンソメも入れる。コンソメが全体に馴染んだらご飯を入れる。\n3　水気がなくなったら出来上がり＊味が濃かったらオムレツを乗せてもよし!!',
	'', 
	'https://cookpad.com/recipe/1016906', 
	1, 
	1
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'丸ごとピーマンのピリ辛蒸し', 
	'new_xl_1569998432_2031.jpg', 
	'にんにくと赤とうがらしがきいたピーマンの蒸し煮。温かいままでも、冷やしてもおいしくいただけます。',
	'2人分', 
	'・ピーマン12コ\n・にんにく1かけ\n・赤とうがらし1～2本\n・エクストラバージンオリーブ油大さじ1+1/2\n・塩小さじ1+1/2\n・こしょう適量', 
	'1　にんにくは縦半分に切り、包丁の腹でつぶす。赤とうがらしは切って種を抜く。ピーマンは洗う。\n2　鍋にオリーブ油 、にんにく、赤とうがらし、塩、こしょう、水カップ1/2を入れ、ピーマンもそのまま入れる。\n3　ふたをして中火にかけ、グツグツと音がしてきたら弱火にし、時々ふたを開けて手早くかき混ぜ、ピーマンがしんなりするまで10～15分間火を通す。途中水分が蒸発してしまったら、水を少し足して火を通すとよい。',
	'とうがらしの種は辛いので抜いておく。切って少しふるだけで取れる。\nピーマンのヘタと種が柔らかければ丸ごと食べてよいが、堅い場合は取り除くとよい。', 
	'https://cookpad.com/recipe/1016906', 
	1, 
	2
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'ターサイの白麻婆豆腐', 
	'4d6f7c96329f3ca795478052588847be.jpg', 
	'あっさりだけどコクのある麻婆豆腐。ターサイがたっぷり食べられます。どんぶりにしてもおいしいです♪',
	'?人分', 
	'ターサイ２束\n豚挽き肉１５０ｇ\n豆腐(木綿でも絹でも)１丁\nしょうが(みじん切り)小さじ１\nにんにく(みじん切り)小さじ１\n長ねぎ(みじん切り)小さじ１\n鷹の爪１/２さや\nサラダ油大さじ２\n\n◆紹興酒大さじ１\n◆てんさいとう小さじ１\n◆鶏がらスープの素小さじ２\n◆塩小さじ１/２\n◆水１カップ\n\nごま油大さじ１くらい\n水溶き片栗粉大さじ１', 
	'1　ターサイは３,４ｃｍの長さに切る。豆腐は1.5ｃｍ角のさいころに切る。\n2　フライパンにサラダ油を入れ、しょうが、にんにく、長ネギ、鷹の爪を弱火で香りを出す。\n3　中火にして豚挽き肉を加えて、ポロポロになるまで炒める。\n4　◆を入れ強火にし、煮立ったら、①のターサイと豆腐を加え、ごま油をたらーりかけて、ふたをする。\n5　2,3分蒸して、ターサイがしんなりしたら、ざっと混ぜ合わせ、水溶き片栗粉を入れる。\n6　汁の泡がぶくぶくして透明になったらできあがり！',
	'我が家は、幼児がいるので、鷹の爪は水につけてから使います。だいぶ、辛味は抜け、香りだけ残ります。大人だけの場合は、１さや使ったり、小口に切ってもいいかもしれません。', 
	'https://cookpad.com/recipe/2733685', 
	1, 
	2
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'ふわふわキャロットケーキ', 
	'new_xl_18155_12460.jpg', 
	'にんじんたっぷりでヘルシーなうえ、一度加熱することで野菜臭さは消え、爽やかなレモン風味の仕上がりに。シナモン風味の生クリームを添えてどうぞ。',
	'直径18cmの丸型1台分', 
	'【にんじんのレモン煮】\n・にんじん （大）1本（180～200g）\n【A】\n・レモン汁大さじ1\n・砂糖大さじ2\n・バター （食塩使用のもの）50g\n・レモンの皮 （国産/すりおろす）1コ分\n・卵3コ\n・砂糖100g\n【B】\n・薄力粉130g\n・ベーキングパウダー小さじ1\n【トッピング】\n・生クリーム100ml\n・砂糖大さじ1\n・シナモン （粉）少々', 
	'前準備\n1 卵は室温に戻し、卵黄と卵白に分けておく。\n2 砂糖はふるっておく。\n3 【B】の薄力粉とベーキングパウダーは合わせてサッと混ぜておく。\n4 オーブンを200℃に温めておく。\n5 丸型にオーブン用の紙を敷いておく。\n1 にんじんは皮をむき、さらに50g分をピーラーで削り、横にして細切りにする。残りはすりおろす（すりおろしたものが約100gになる）。\n2 耐熱ボウルに1と【A】を入れて混ぜ、ふんわりとラップをし、電子レンジ（600W）に3分間かけたあと、触れる程度まで冷ます。\n3 バターは耐熱容器に入れ、湯煎、またはラップをして電子レンジ（600W）に50秒～1分間かけて溶かす。2と、すりおろしたレモンの皮を合わせる。\n4 大きなボウルに卵白を入れ、ハンドミキサーを強にして1分間泡立てる。フワッとしてきたところで、砂糖を2～3回に分けて加える。\n5 さらにハンドミキサーで3分間ほど泡立て、重くてしなやかな「鳥のくちばし」状にする。\n6 卵黄を加えて、さらにハンドミキサーで10～15秒間ほど、全体を混ぜる（少しムラがあってもよい）。\n7 3に6をゴムべらで2回すくい入れ、ハンドミキサーで10秒間ほど、全体をムラなく混ぜる。\nこのとき、3のバターとにんじんが冷たくなっていると、ふんわりと焼き上がらないので、その場合は電子レンジに少しかけて温め直す。\n8 残りの6に【B】の半量弱をふるい入れ、ゴムべらで混ぜ合わせる。\nボウルを左手で反時計回りに回しながら、ゴムべらは時計回りに縁に向かって切るように混ぜるとよい。\n9 ほぼ粉が見えなくなるくらいに混ざったら、7を加えて同様に混ぜる。9割混ざったら、残りの【B】をふるい入れ、全体を混ぜる。\n薄力粉は2回に分けて混ぜるとしっかり混ぜても卵の泡がつぶれにくく、生地が均一にふくらむ。\n10 オーブン用の紙を敷いた丸型に、生地を流し入れる。\n11 200℃のオーブンで10分間焼き、170℃に下げてさらに20～30分間焼く。竹串を刺してみて、生地がつかなければOK。\n12 丸型に入れたまま乾いた布巾をかぶせ、皿などをのせて冷ます。型からはずし、適当な大きさに切って皿に盛る。トッピングの生クリームに砂糖とシナモンを混ぜて泡立て、シナモンをふる。',
	'にんじんの苦手な人は、すりおろし120gのみでレモン煮をつくってもよい。しっかり冷ましたら、冷蔵庫で3～4日間、冷凍庫で約1か月間保存可能。', 
	'https://cookpad.com/recipe/1016906', 
	1, 
	3
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'材料３つ！超簡単オールドファッション風', 
	'', 
	'卵もバターも使わず、作り方も簡単なサクサクのドーナツです(*p´u｀q*)',
	'5個くらい', 
	'ホットケーキミックス80ｇ\n片栗粉20ｇ\nはちみつ20ｇ\n水20ｃｃ', 
	'1　材料をすべてボウルに入れ、ゴムベラ等で混ぜ、大体混ざったら手で捏ねてしっかり混ぜる。\n2　油を１８０℃に温めておく。\n3　１を麺棒で1センチくらいに伸ばし、ドーナツ型に抜く。\n内円と外円の間に1周傷を付けておくと、綺麗な割れ目が出来ます。\n4　３を両面綺麗な焼き色が付くまで揚げる。',
	'３の時、打ち粉はしなくても出来ますが、した方がやりやすいと思います（私は面倒なのでしませんでした^^;）。\n丸めてサーターアンダギー風にする場合は、160℃くらいにして、少し時間をかけて揚げた方が中まで火が通ると思いますｗ', 
	'https://cookpad.com/recipe/1002256', 
	1, 
	3
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'丸ごとブロッコリーごはん', 
	'3a1aad4bd78a224257d91e8a4ee962d5.jpg', 
	'すごいインパクトです！',
	'2合', 
	'お米2合\nブロッコリー1本\nツナ缶1缶\n醤油大さじ2\nみりん大さじ2\n顆粒和風だし大さじ1', 
	'1　お米をといだら、炊飯器に醤油、みりん、顆粒和風だしを加え、メモリまで水を入れる。\n2　ブロッコリーは茎を切り落とし、茎は厚めに皮をむいて千切りにする。\n3　ツナ缶と２を炊飯器に入れたらブロッコリーの茎以外の部分を真ん中にさす。\n4　通常炊きで完成。',
	'茎の皮を厚めに剥きましょう。残っていると筋っぽくて食べるとき大変です。', 
	'https://cookpad.com/recipe/4244234', 
	1, 
	1
);

INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id
) VALUES (
	'シメジ入りハンバーグ', 
	'i=https%3A%2F%2Fimage.excite.co.jp%2Fjp%2Ferecipe%2Frecipe%2Fd%2Fa%2Fda16123dc388d3dff9bfb06d5b147751%2F6c60f25bc1e75c4b1ce7c8b4b2df39d3.jpg', 
	'シメジが苦手な人でも、ハンバーグに混ぜ込んでしまえば食べれるかも。',
	'4人分', 
	'合いびき肉350～400g\n玉ネギ1/2個\nシメジ1袋\nパン粉大さじ6～7\n牛乳大さじ3\nサラダ油大さじ1\n生クリーム大さじ1\n卵1個\n塩コショウ少々\nナツメグ少々\nドライパセリ少々\n\n<ソース>\n  赤ワイン大さじ3\n  ケチャップ大さじ3\n  しょうゆ大さじ1\n  バター10g\n  ウスターソース大さじ1\n  塩コショウ少々', 
	'下準備\n玉ネギ、シメジは石づきを切り落とし、それぞれみじん切りにする。フライパンに分量外のサラダ油少々を熱し、しんなりするまで炒め合わせて冷ましておく。\n1 ボウルに合いびき肉、塩コショウ、ナツメグを入れよく混ぜ合わせる。炒めた玉ネギとシメジ、パン粉、牛乳、生クリーム、卵を加え、全体に混ぜ合わせる。\n2 手に分量外のサラダ油をぬり、(1)を8等分にし、1個ずつキャッチボールをする要領で空気を抜き、小判型に形を整える。\n3 フライパンにサラダ油を強火で熱し、ハンバーグを並べ入れる。両面をそれぞれ40秒程焼いて焼き色を付け、うまみを封じ込める。\n4 火を弱めて、蓋をして5～6分蒸し焼きにする。ハンバーグの中央に竹串を刺し、澄んだ汁が出たらOK！ハンバーグはいったん取り出しておく（濁った赤い汁ならまだ焼けていないので、もう一度蓋をして焼く）。\n5 (4)のフライパンに＜ソース＞の赤ワインを煮立たせ、他の材料を加えてひと煮立ちさせ、ハンバーグを戻し、ソースをからめる。器に盛り、ドライパセリを振る。',
	'お好みでいろんなキノコをブレンドして加えてもいいですね！', 
	'https://erecipe.woman.excite.co.jp/detail/da16123dc388d3dff9bfb06d5b147751.html', 
	2, 
	3
);

INSERT INTO recipe_note.t_category (category_name, user_id) VALUES ('お菓子', 2),('ごはん', 2),('おかず', 2);

INSERT INTO recipe_note.t_tag (tag_name,user_id) VALUES ('片栗粉',1);

INSERT INTO recipe_note.t_recipe_tag (recipe_id, tag_id) VALUES (1, 1),(2, 2),(2, 3),(4, 4),(6, 4);

UPDATE recipe_note.t_recipe_tag SET tag_id = 6 WHERE recipe_tag_id= 4;

--表示数ぶん取得
SELECT recipe_id, recipe_name, image, description, category_id, favorite_id 
FROM recipe_note.t_recipe 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (recipe_id) 
WHERE recipe_note.t_recipe.user_id = 1 
ORDER BY recipe_id ASC LIMIT 0, 6;

SELECT recipe_id, recipe_name, image, description, favorite_id
FROM recipe_note.t_recipe 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (recipe_id) 
WHERE category_id = 3 
ORDER BY recipe_id ASC LIMIT 0, 6;

SELECT recipe_id, recipe_name, image, description, favorite_id 
FROM recipe_note.t_recipe_tag 
LEFT OUTER JOIN recipe_note.t_recipe 
USING (recipe_id) 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (recipe_id) 
WHERE recipe_note.t_recipe_tag.tag_id = 6 
ORDER BY recipe_id ASC LIMIT 0, 6;

--総数を取得
SELECT count(recipe_id) FROM recipe_note.t_recipe WHERE user_id = 1;
SELECT count(recipe_id) FROM recipe_note.t_recipe WHERE category_id = 1;


UPDATE recipe_note.t_recipe SET category_id = 6 WHERE recipe_id = 8;

--カテゴリテーブルとタグテーブルにユーザーIDカラムを追加
ALTER TABLE recipe_note.t_category ADD user_id int NOT NULL;
UPDATE recipe_note.t_category SET user_id = 1;

ALTER TABLE recipe_note.t_tag ADD user_id int NOT NULL;
UPDATE recipe_note.t_tag SET user_id = 2 WHERE tag_id = 4 or tag_id = 5;

--ユーザーIDからカテゴリ名を取得
SELECT * FROM recipe_note.t_category WHERE user_id = 1;
SELECT * FROM recipe_note.t_category WHERE user_id = 1 AND category_name <> '未分類';
--ユーザーIDからタグ名を取得
SELECT * FROM recipe_note.t_tag WHERE user_id = 1;

--レシピIDから詳細を取得
SELECT * FROM recipe_note.t_recipe WHERE recipe_id = 3;

--レシピIDから詳細とカテゴリ名とタグIDとタグ名を取得
SELECT *
FROM recipe_note.t_recipe r
LEFT OUTER JOIN recipe_note.t_category c
USING (category_id)
LEFT OUTER JOIN recipe_note.t_recipe_tag rt
USING (recipe_id)
LEFT OUTER JOIN recipe_note.t_tag t
USING (tag_id)
WHERE r.recipe_id = 5;

--レシピIDから詳細とカテゴリ名とFavIDを取得
SELECT 	*
FROM recipe_note.t_recipe r
LEFT OUTER JOIN recipe_note.t_category
USING (category_id)
LEFT OUTER JOIN recipe_note.t_favorite
USING (recipe_id)
WHERE r.recipe_id = 2;

--レシピIDからタグIDとタグ名を取得
SELECT *
FROM recipe_note.t_recipe_tag rt
LEFT OUTER JOIN recipe_note.t_tag t
USING (tag_id)
WHERE rt.recipe_id = 2;

--タグIDからレシピ詳細を取得
SELECT * 
FROM recipe_note.t_recipe_tag rt
LEFT OUTER JOIN recipe_note.t_recipe r
USING (recipe_id)
WHERE rt.tag_id = 5 
ORDER BY recipe_id ASC LIMIT 0, 6;

SELECT count(recipe_id) AS cnt FROM recipe_note.t_recipe_tag WHERE tag_id = 6;

INSERT INTO recipe_note.t_category (category_name,user_id) VALUES ('未分類',1),('未分類',2),('未分類',3);

--該当ユーザーの未分類カテゴリに新しいレシピを登録
INSERT INTO recipe_note.t_recipe (
	recipe_name,
	image,
	description,
	servings,
	ingredients,
	preparation,
	tips,
	source,
	user_id,
	category_id 
) SELECT 
	'てすとごはん', 
	'test.jpg', 
	'てすとです。', 
	'2合', 
	'てすと1個', 
	'てすと', 
	'てすとです。', 
	'テストブック', 
	3, 
	category_id 
FROM
	recipe_note.t_category 
WHERE
	user_id = 3 AND category_name = '未分類';

--レシピ更新
UPDATE recipe_note.t_recipe
SET recipe_name = 'test curry',
	image = 'no-image2.png',
	description = 'てすとです。',
	servings = '2合',
	ingredients = 'てすと1個',
	preparation = 'てすと',
	tips = 'てすとです。',
	source = 'テストブック',
	user_id = 1,
	category_id = 3 
WHERE
	recipe_id = 18;


--レシピに既存タグを登録
INSERT INTO recipe_note.t_recipe_tag (recipe_id, tag_id) VALUES (6, 6);

--FavoriteDao
INSERT INTO recipe_note.t_favorite (user_id, recipe_id) VALUES (1, 26);
SELECT * FROM recipe_note.t_favorite WHERE recipe_id = 25;
DELETE FROM recipe_note.t_favorite WHERE recipe_id = 14;

--ユーザーIDとカテゴリ名からカテゴリ/タグIDを検索
SELECT category_id FROM recipe_note.t_category WHERE user_id = 1 AND category_name = 'つまみ';
SELECT tag_id FROM recipe_note.t_tag WHERE user_id = 1 AND tag_name = 'いも'

--syntax errorになる→ならなくなった。。
INSERT INTO table_name (col_1, col_2, col_3) VALUES (1, 2, 3), (4, 5, 6), (7, 8, 9);
INSERT INTO recipe_note.t_favorite (user_id, recipe_id) VALUES (1, 2),(1, 5),(1, 6),(1, 11);

UPDATE recipe_note.t_recipe SET category_id = 20, user_id = 1 WHERE recipe_id = 29;

DELETE FROM recipe_note.t_category WHERE category_id = 14 OR category_id = 16;

--削除対象カテゴリに紐づいたレシピを未分類カテゴリに紐づけ直す
UPDATE recipe_note.t_recipe 
SET category_id = (SELECT category_id FROM recipe_note.t_category WHERE user_id = 1 AND category_name = '未分類')
WHERE category_id = 10 OR category_id = 12;

--削除対象タグをタグテーブルとレシピタグテーブルから削除
DELETE recipe_note.t_tag, recipe_note.t_recipe_tag
FROM recipe_note.t_tag 
LEFT OUTER JOIN recipe_note.t_recipe_tag 
USING (tag_id) 
WHERE tag_id = 27;

--削除対象レシピをレシピテーブルとレシピタグテーブルとお気に入りテーブルから削除
DELETE recipe_note.t_recipe, recipe_note.t_recipe_tag, recipe_note.t_favorite
FROM recipe_note.t_recipe 
LEFT OUTER JOIN recipe_note.t_recipe_tag 
USING (recipe_id) 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (recipe_id) 
WHERE recipe_id = 57;

--削除対象ユーザーIDに紐づいた全データ削除
DELETE recipe_note.t_user, recipe_note.t_recipe, recipe_note.t_category, recipe_note.t_recipe_tag, recipe_note.t_tag, recipe_note.t_favorite
FROM recipe_note.t_user 
LEFT OUTER JOIN recipe_note.t_recipe 
USING (user_id) 
LEFT OUTER JOIN recipe_note.t_category 
USING (user_id) 
LEFT OUTER JOIN recipe_note.t_recipe_tag 
USING (recipe_id) 
LEFT OUTER JOIN recipe_note.t_tag 
USING (user_id) 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (user_id) 
WHERE user_id = 6;

SELECT user_id, r.recipe_id,c.category_id,recipe_tag_id,t.tag_id,favorite_id
FROM recipe_note.t_user 
LEFT OUTER JOIN recipe_note.t_recipe r
USING (user_id) 
LEFT OUTER JOIN recipe_note.t_category c
USING (user_id) 
LEFT OUTER JOIN recipe_note.t_recipe_tag 
USING (recipe_id) 
LEFT OUTER JOIN recipe_note.t_tag t
USING (user_id) 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (user_id) 
WHERE user_id = 2;

--フォーム検索(ヒント：fav_id!=0)
SELECT favorite_id, recipe_id, recipe_name, image, description
FROM recipe_note.t_recipe r
LEFT OUTER JOIN recipe_note.t_recipe_tag rt
USING (recipe_id)
LEFT OUTER JOIN recipe_note.t_favorite
USING (recipe_id)
WHERE r.user_id = 1 AND tag_id = 6 AND recipe_name LIKE '%t%' AND recipe_name LIKE '%s%'
ORDER BY recipe_id ASC LIMIT 0, 6;

SELECT *
FROM recipe_note.t_tag t
LEFT OUTER JOIN recipe_note.t_recipe_tag rt
USING (tag_id)
WHERE tag_id = 6;

SELECT DISTINCT favorite_id, recipe_id, recipe_name, image, description 
FROM recipe_note.t_recipe r 
LEFT OUTER JOIN recipe_note.t_recipe_tag 
USING (recipe_id) 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (recipe_id) 
WHERE r.user_id = 1 AND favorite_id <> 0 
ORDER BY recipe_id ASC LIMIT 0, 6;

tag_id IN (2, 3) == tag_id = 2 OR tag_id = 3;

SELECT count(DISTINCT recipe_id) AS cnt 
FROM recipe_note.t_recipe r 
LEFT OUTER JOIN recipe_note.t_recipe_tag 
USING (recipe_id) 
LEFT OUTER JOIN recipe_note.t_favorite 
USING (recipe_id) 
WHERE r.user_id = 1 AND favorite_id <> 0;

--タグAND検索版
SELECT DISTINCT favorite_id, r.recipe_id, recipe_name, image, description 
FROM recipe_note.t_recipe r 
LEFT OUTER JOIN recipe_note.t_favorite f
ON r.recipe_id = f.recipe_id 
INNER JOIN recipe_note.t_recipe_tag rt1
ON r.recipe_id = rt1.recipe_id AND rt1.tag_id = 3 
INNER JOIN recipe_note.t_recipe_tag rt2
ON r.recipe_id = rt2.recipe_id AND rt2.tag_id = 2 
WHERE r.user_id = 1 AND favorite_id <> 0 
ORDER BY r.recipe_id ASC LIMIT 0, 6;

SELECT count(DISTINCT r.recipe_id) AS cnt 
FROM recipe_note.t_recipe r 
LEFT OUTER JOIN recipe_note.t_favorite f
ON r.recipe_id = f.recipe_id 
INNER JOIN recipe_note.t_recipe_tag rt1
ON r.recipe_id = rt1.recipe_id AND rt1.tag_id = 3 
INNER JOIN recipe_note.t_recipe_tag rt2
ON r.recipe_id = rt2.recipe_id AND rt2.tag_id = 2 
WHERE r.user_id = 1 AND favorite_id <> 0;


UPDATE recipe_note.t_category 
SET category_name =
CASE category_id
WHEN 7 THEN 'ごはんモノ'
WHEN 13 THEN '粉モノ'
END
WHERE category_id IN (7,13);

UPDATE recipe_note.t_tag 
SET tag_name =
CASE tag_id
WHEN 10 THEN '長いも'
WHEN 15 THEN '薄力粉'
END
WHERE tag_id IN (10,15);

UPDATE recipe_note.t_recipe_tag 
SET recipe_id = 28 
WHERE recipe_tag_id IN (37,38,39,40);
